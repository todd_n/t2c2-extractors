#!/bin/bash

# add/replace if variable is non empty
# $1 = variable to replace/remove
# $2 = new value to set
function update_conf() {
    local query
    if [ "$1" == "" ]; then return 0; fi

    # First remove existing configuration info
    if [ -e /code/sem_extractor_config.py ]; then
        if [ "$2" != "" ]; then
            query="$1"
	    mv /code/sem_extractor_config.py /code/sem_extractor_config.py.old
            grep -v "^$query" /code/sem_extractor_config.py.old > /code/sem_extractor_config.py
            rm /code/sem_extractor_config.py.old
	fi
    fi

    # Then, update config info
    if [ "$2" != "" ]; then
        echo "$1=\"$2\"" >> /code/sem_extractor_config.py
    fi
}

# Set configuration information 
update_conf   "rabbitmqhost" "$RABBITMQ_URL"
update_conf   "clowderhost" "$CLOWDER_URL"
update_conf   "clowderport" "$CLOWDER_PORT"
update_conf   "credentials_name" "$CRED_NAME"
update_conf   "credentials_pass" "$CRED_PASS"
update_conf   "playserverKey" "$SERVER_KEY"
update_conf   "exchange_name" "$EXCHANGE_NAME"
update_conf   "use_ssl" "$USE_SSL"
update_conf   "messageType" "$MESSAGE_TYPE"

# Start clowder
cd /code
python sem_extractor.py

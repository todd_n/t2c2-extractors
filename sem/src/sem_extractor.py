import pyclowder
import sys
import time
import pika
import json
import logging
import pyclowder.extractors as extractors
import sem_extractor_config
import requests
import os
from sem_extractor_config import *

def main():
    global extractorName, messageType, rabbitmqExchange, rabbitmqURL, registrationEndpoints,logger

    receiver = 'sem.metadata'
    #set logging
    extractorName = 'sem.metadata.extractor'
    messageType = "*.dataset.file.added"
    rabbitmqExchange = sem_extractor_config.exchange_name
    rabbitmqURL = "amqp://guest:guest@" + sem_extractor_config.rabbitmqhost + ":5672/%2f"
    host = sem_extractor_config.clowderhost
    port = sem_extractor_config.clowderport
    playserverKey = sem_extractor_config.playserverKey

    logging.basicConfig(format='%(levelname)-7s : %(name)s -  %(message)s', level=logging.WARN)
    logging.getLogger('pyclowder.extractors').setLevel(logging.INFO)

    logger = logging.getLogger(receiver)
    logger.setLevel(logging.DEBUG)

    n_retries = 0
    while(True):
        try:
            #connect to rabbitmq
            extractors.connect_message_bus(
                    extractorName=extractorName, 
                    messageType=messageType, 
                    processFileFunction=process_dataset, 
                    checkMessageFunction=check_message, 
                    rabbitmqExchange=rabbitmqExchange, 
                    rabbitmqURL=rabbitmqURL)
            break
        except pika.exceptions.AMQPConnectionError:
            n_retries += 1
            if n_retries == 5:
                break
            print 'Cannot connect to RabbitMQ server. Retry in 5s...'
            time.sleep(5)


def check_message(parameters):
    return True


def findMatches(relevant_tif_files,relevant_text_files):
    tif_names = relevant_tif_files.keys()
    text_names = relevant_text_files.keys()
    shared_names = set(tif_names).intersection(set(text_names))
    return shared_names



def addMetadataToTifFile(parameters,host,tif_file,metaDataJson):
    tif_id = tif_file['id']
    headers = {'Content-Type': 'application/json'}
    url = host +'/api/files/' + tif_id + '/metadata?key=' + parameters['secretKey']

    r = requests.post(url, headers=headers, data=metaDataJson, verify=False)
    r.raise_for_status()

def process_dataset(parameters):
    files = parameters['filelist']
    the_files = extractors.get_dataset_file_list(parameters['datasetId'],parameters)
    #for each in the_files:
        #process_file(each,the_files,parameters)

    for each in files:
        process_file(each,parameters)
    """
    last_file = parameters['filelist'][-1]
    last_file_name = last_file['filename']
    last_file_id = last_file['id']
    matching_file = findMatchingFile(last_file,files)
    host = parameters['host']
    if '.txt' in last_file_name.lower():
        name = last_file_name[0:-4]
        metadata = getMetadataAsJsonFromTextFile(matching_file)
        matching_tifs = findMatchingTifFiles(name,parameters)
        for current_tif in matching_tifs:
            file_md = extractors.download_file_metadata_jsonld(parameters['host'], parameters['secretKey'],
                                                               current_tif['id'])
            if len(file_md) > 0:
                for md in file_md:
                    if 'content' in md:
                        mdc = md['content']
                        if 'log file added' not in mdc:
                            addMetadataToTifFile(parameters, host, current_tif, metadata)
                        else:
                          pass
            else:
                addMetadataToTifFile(parameters,host,current_tif, metadata)
    elif '.tif' in last_file_name.lower():
        name = last_file_name[0:last_file_name.index('.tif')]
        matching_texts = findMatchingTextFilesForTif(name+'.txt',parameters['files'])
        if (len(matching_texts) > 0):
            #only add if it doesn't have metadata
            file_md = extractors.download_file_metadata_jsonld(parameters['host'], parameters['secretKey'],
                                                               last_file_id)
            if len(file_md) > 0:
                for md in file_md:
                    if 'content' in md:
                        mdc = md['content']
                        if 'log file added' not in mdc:
                            metadataJson = getMetadataAsJsonFromTextFile(matching_texts[0])
                            addMetadataToTifFile(parameters, host, last_file, metadataJson)
                        else:
                            pass
    """

def process_file(current_file,parameters):
    addedLogFile = False
    files = parameters['files']
    last_file = current_file
    last_file_name = last_file['filename']
    last_file_id = last_file['id']
    matching_file = findMatchingFile(last_file,files)
    host = parameters['host']
    if '.txt' in last_file_name.lower():
        name = last_file_name[0:-4]
        metadata = getMetadataAsJsonFromTextFile(matching_file)
        matching_tifs = new_findMatchingTifFiles(name,parameters)
        for current_tif in matching_tifs:
            file_md = extractors.download_file_metadata_jsonld(parameters['host'], parameters['secretKey'],
                                                               current_tif['id'])
            if len(file_md) > 0:
                for md in file_md:
                    if 'content' in md:
                        mdc = md['content']
                        if 'log file added'  in mdc:
                            addedLogFile = True
                        else:
                          pass
                if (addedLogFile):
                    pass
                else:
                    addMetadataToTifFile(parameters, host, current_tif, metadata)
            else:
                addMetadataToTifFile(parameters,host,current_tif, metadata)
    elif '.tif' in last_file_name.lower():
        name = last_file_name[0:last_file_name.index('.tif')]
        matching_texts = new_findMatchingTextFilesForTif(name+'.txt',parameters['files'])
        if (len(matching_texts) > 0):
            #only add if it doesn't have metadata
            file_md = extractors.download_file_metadata_jsonld(parameters['host'], parameters['secretKey'],
                                                               last_file_id)
            if len(file_md) > 0:
                for md in file_md:
                    if 'content' in md:
                        mdc = md['content']
                        if 'log file added' in mdc:
                            addedLogFile = True
                if (addedLogFile):
                    pass
                else:
                    metadataJson = getMetadataAsJsonFromTextFile(matching_texts[0])
                    addMetadataToTifFile(parameters, host, last_file, metadataJson)
                    addedLogFile = True
            else:
                metadataJson = getMetadataAsJsonFromTextFile(matching_texts[0])
                addMetadataToTifFile(parameters, host, last_file, metadataJson)
                addedLogFile = True

def getMetadataAsJsonFromTextFile(text_file):
    f = open(text_file,'r')
    lines = f.readlines()
    f.close()
    metadata = dict()
    allEqualSeparatedLines = equalSeparatedLines(lines)
    if (allEqualSeparatedLines):
        for line in lines:
            try:
                current_line = line.split('=')
                unprocessed_key = current_line[0]
                key = current_line[0].encode('utf-8').strip()
                if ("_" in key):
                    key = key[key.index('_') + 1:]
                value_text = line[(len(unprocessed_key)):]
                if (value_text[0] == "="):
                    value_text = value_text[1:]
                value = (value_text.encode('utf-8')).strip()
                metadata[key] = value
            except:
                pass
    else:
        for line in lines:
            try:
                current_line = line.split(' ')
                unprocessed_key = current_line[0]
                key = current_line[0].encode('utf-8').strip()
                key = key[key.index('_') + 1:]
                value_text = line[(len(unprocessed_key)):]
                value = ((line[len(unprocessed_key):]).encode('utf-8')).strip()
                metadata[key] = value
            except:
                pass
    if (len(metadata) > 0):
        metadata['extractor run'] = 'true'
        metadata['log file added'] = 'true'
    elif (len(metadata) == 0 and len(lines)>0):
        metadata['extractor run'] = 'true'
    jsonArray = json.dumps(metadata)
    return jsonArray


def equalSeparatedLines(lines):
    allEquals = True
    notEqualsLines = 0
    for line in lines:
        if ('=' not in line):
            notEqualsLines+=1
    if (notEqualsLines > 2):
        allEquals = False
    return allEquals

def findMatchingFile(last_file,file_list):
    last_file_name = last_file['filename']
    last_file_id = last_file['id']
    matching_file = None
    for each_file in file_list:
        if last_file_name in each_file:
            if last_file_id in each_file:
                if each_file[-4:] != 'json':
                    matching_file = each_file
    return matching_file




def findMatchingTextFilesForTif(name,file_list):
    matching_files = []
    for each_file in file_list:
        if name in each_file:
            if each_file[-4:] != 'json':
                matching_file = each_file
                matching_files.append(matching_file)
    return matching_files


def new_findMatchingTextFilesForTif(name,file_list):
    matching_files = []
    for each_file in file_list:
        if '/'+name in each_file:
            if each_file[-4:] != 'json':
                matching_file = each_file
                matching_files.append(matching_file)
    return matching_files

def findMatchingTifFiles(name,parameters):
    tif_files = []
    file_list = parameters['filelist']
    for current_file in file_list:
        current_file_name = current_file['filename']
        if name in current_file_name and '.tif' in current_file_name:
            tif_files.append(current_file)
    return tif_files

def new_findMatchingTifFiles(name,parameters):
    tif_files = []
    file_list = parameters['filelist']
    for current_file in file_list:
        current_file_name = current_file['filename']
        if '/'+name in current_file_name and '.tif' in current_file_name:
            tif_files.append(current_file)
    return tif_files



if __name__ == "__main__":
    main()

import collections

def flatten_dictionary(d, parent_key='', sep='_'):
    items = []
    for k, v in d.items():
        #new_key = parent_key + sep + k if parent_key else k
        new_key = k
        if isinstance(v, collections.MutableMapping):
            items.extend(flatten_dictionary(v, new_key, sep=sep).items())
        else:
            if type(v) is not list:  # only add if value is not a list
                if new_key.find("(") != -1: # it means that there is a unit
                    unit = new_key[new_key.find("(") + 1:new_key.find(")")] # get what is in the parentheses
                    new_key = new_key[:new_key.find("(") - 1] # only parameter name without unit
                    v = str(v) + " " + unit
                items.append((new_key, v))

    return dict(items)


#tags should be fixed - no periods, underscores, and trimmed
def get_metadata_shortlist(**tags):
    #need more research on the top 10 fields and what counts

    """
    Beam Voltage
    Beam Current
    Spot Size
    Alpha
    Magnification/camera length
    Condenser Aperture
    Objective Aperture
    Dark/bright filed
    Exposure
    Binning
    Specimen

    """

    new_shortlist_keys = ['beam','voltage','current','spot','alpha','magnification','camera length','condenser',
                         'aperture','objective','dark','bright','exposure','binning','specimin']

    shortlist_keys = ['actual magnification', 'emission current', 'illumination mode', 'imaging mode',
                      'magnification interpolated', 'microscope info microscope', 'operation mode',
                      'indicated magnification','voltage']
    keys = tags.keys()
    short_metadata = dict()
    for key in keys:
        lower_key = key.lower()
        for each in new_shortlist_keys:
            if each in lower_key:
                #new_key = convert_encoding(key,new_encoding="UTF-8")
                old_value = tags[key]
                new_value =  old_value#convert_encoding(old_value,new_encoding="UTF-8")
                short_metadata[key] = new_value
    return short_metadata

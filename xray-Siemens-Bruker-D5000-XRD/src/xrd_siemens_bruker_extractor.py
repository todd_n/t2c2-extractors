import extractor_config
import xrd_siemens_bruker_D5000
import logging
import os
import requests
import json
import pyclowder.extractors as extractors
import tempfile

def main():
    global extractorName, messageType, rabbitmqExchange, rabbitmqURL, registrationEndpoints,logger

    receiver = 'xrd-powder-extractor'
    extractorName = extractor_config.extractorName
    messageType =  extractor_config.messageType
    rabbitmqExchange =extractor_config.rabbitmqExchange
    rabbitmqURL = extractor_config.rabbitmqURL
    playserverKey =extractor_config.playserverKey


    logging.basicConfig(format='%(levelname)-7s : %(name)s -  %(message)s', level=logging.WARN)
    logging.getLogger('pyclowder.extractors').setLevel(logging.INFO)

    logger = logging.getLogger(receiver)
    logger.setLevel(logging.DEBUG)

    extractors.connect_message_bus(extractorName=extractorName, messageType=messageType, processFileFunction=processFile,
        checkMessageFunction=check_message, rabbitmqExchange=rabbitmqExchange, rabbitmqURL=rabbitmqURL)

def check_message(parameters):
    return True

def processFile(parameters):
    input_file = parameters['inputfile']
    lines = xrd_siemens_bruker_D5000.getLines(input_file)

    if (len(lines) > 1):
        if ('RAW' in lines[0] and 'RawHeader' in lines[1]):

            (fd, tn_file) = tempfile.mkstemp(suffix=".png")
            # create image preview
            try:
                metadata = xrd_siemens_bruker_D5000.getMetadata(lines)
                xrd_siemens_bruker_D5000.makePlot(lines, tn_file)
                preview_id = extractors.upload_preview(tn_file, parameters, None)
                upload_preview_title(preview_id, 'Plot of All Points', parameters)
                try:
                    addMetadataToXRDFile(parameters, parameters['host'], parameters['fileid'], json.dumps(metadata))
                except:
                    logger.error("could not post metadata")
            except:
                logger.error("could not process file " + input_file)

            try:
                os.remove(tn_file)
            except:
                logger.error("Could not remove temporary file")

            try:
                os.remove(tn_file)
            except:
                logger.error("Could not remove temporary file")

def upload_preview_title(previewid, title_name,parameters):
    key = parameters['secretKey']
    title = dict()
    title['title'] = title_name
    host = parameters['host']
    if (not host.endswith("/")):
        host = host + "/"
    url = host + 'api/previews/'+previewid+'/title?key='+key

    headers = {'Content-Type': 'application/json'}

    if url:
        r = requests.post(url, headers=headers, data=json.dumps(title), verify=False);
        r.raise_for_status()
    return r

def addMetadataToXRDFile(parameters,host,file_id,metaDataJson):
    headers = {'Content-Type': 'application/json'}
    url = host +'/api/files/' + file_id + '/metadata?key=' + parameters['secretKey']

    r = requests.post(url, headers=headers, data=metaDataJson, verify=False)
    r.raise_for_status()

if __name__ == "__main__":
    main()

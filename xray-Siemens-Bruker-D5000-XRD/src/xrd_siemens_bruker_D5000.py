import matplotlib
matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors
import six
import re

def getLines(current_file):
    f = open(current_file,'r')
    lines = f.readlines()
    f.close()
    for i in range(0,len(lines)):
        lines[i] = lines[i].rstrip()
    return lines

def findMetadataHeadings(lines_from_file):
    keys = []
    for line in lines_from_file:
        if ('[' in line and ']' in line and line != '[Data]'):
            keys.append(line)
    keys = set(keys)
    keys = list(keys)
    return keys


def getMetadata(lines_from_file):
    metadata = dict()
    metadata_keys = findMetadataHeadings(lines_from_file)
    metadata = dict()
    for key in metadata_keys:
        current_key_dict = dict()
        metadata[key] = current_key_dict
    current_key_value = None
    for line in lines_from_file:
        if line in metadata_keys:
            current_key_value = line
        elif line == '[Data]':
            return metadata
        elif line != "" and current_key_value != None:
            current_dict = metadata[current_key_value]
            try:
                line = line.split('=')
                current_key = line[0]
                current_value = line[1]
                current_dict[current_key] = current_value
            except:
                pass
        else:
            pass

    return metadata



def getData(lines_from_files):
    start_of_data = lines_from_files.index('[Data]')
    raw_data = lines_from_files[start_of_data+2:]
    data = []
    x_angles = []
    y_intensity = []
    for line in raw_data:
        current = line.replace(' ','')
        current = current.split(',')
        angle = float(current[0])
        intensity = float(current[1])
        x_angles.append(angle)
        y_intensity.append(intensity)
    return [x_angles,y_intensity]

def makePlot(lines_from_file,target_file):
    data = getData(lines_from_file)
    x_angles = np.array(data[0])
    y_intensity = np.array(data[1])
    plt.plot(x_angles, y_intensity, color='red', linestyle='solid')
    plt.xlabel("angle")
    plt.ylabel("intensity")
    plt.savefig(target_file)


""""
lines = getLines(sample_file)

hdings = findMetadataHeadings(lines)

m = getMetadata(lines)

data = getData(lines)

makePlot(lines,'test.png')

print('done')
"""
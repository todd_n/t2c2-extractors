'''

@authors: Constantinos Sophocleous, Inna Zharnitsky
'''

#!/usr/bin/env python
import pika
import sys
import logging
import json
import traceback
import requests
import tempfile
import subprocess
import os
import itertools
import time
from ssl import PROTOCOL_SSLv3

#Note: For the extractor to work, convert.exe in the Imagemagick installation folder has to be changed to imagemagickconvert.exe

def main():
	global logger

	# name of receiver
	receiver='ncsa.imageThumbnail'

	# configure the logging system
	logging.basicConfig(format="%(asctime)-15s %(name)-10s %(levelname)-7s : %(message)s", level=logging.WARN)
	logger = logging.getLogger(receiver)
	logger.setLevel(logging.DEBUG)

	if len(sys.argv) < 8:
		logger.info("Input RabbitMQ server address, followed by RabbitMQ server port,  RabbitMQ username, RabbitMQ password, Medici REST API key, RabbitMQ exchange name and whether SSL is to be used to communicate with the RabbitMQ messaging service (true of false).")
		logger.info("If SSL is used, add the path to the key certificate file and the path to the key file.")
		sys.exit()

	global playserverKey
	playserverKey = sys.argv[5]

	global exchange_name
	exchange_name = sys.argv[6]

	# connect to rabbitmq using input username and password
	credentials = pika.PlainCredentials(sys.argv[3], sys.argv[4])
	host = sys.argv[1]
	port = int(sys.argv[2])

	useSsl = (sys.argv[7] == "true")
	if useSsl:
		sslCertPath = sys.argv[8]
		sslKeyPath = sys.argv[9]

		ssl_options = {"certfile": sslCertPath, "server_side": True, "keyfile": sslKeyPath, "ssl_version": PROTOCOL_SSLv3}

		parameters = pika.ConnectionParameters(credentials=credentials, ssl=useSsl, ssl_options=ssl_options, port=port, host=host)
	else:
		parameters = pika.ConnectionParameters(credentials=credentials, port=port, host=host)
	connection = pika.BlockingConnection(parameters)

	# connect to channel
	channel = connection.channel()

	# declare the exchange
	channel.exchange_declare(exchange=exchange_name, exchange_type='topic', durable=True)

	# declare the queue
	channel.queue_declare(queue=receiver, durable=True)

	# Set prefetch-count to 1 to avoid crashing due to interleaved frames and also send jobs to idle workers first
	channel.basic_qos(prefetch_count=1)

	# connect queue and exchange
	channel.queue_bind(queue=receiver, exchange=exchange_name, routing_key='*.file.image.#')

	# create listener
	channel.basic_consume(on_message, queue=receiver, no_ack=False, consumer_tag="medici_image_thumbnail")

	# start listening
	logger.info("Waiting for messages. To exit press CTRL+C")
	try:
		channel.start_consuming()
	except KeyboardInterrupt:
		channel.stop_consuming()

	# close connection
	connection.close()

def create_image_thumbnail(inputfile, ext, size, host, apipath, fileid, *args):
	global logger
	(fd, thumbnailfile)=tempfile.mkstemp(suffix='.' + ext)
	try:
		# convert image to right size
		#args = [['convert', inputfile, '-resize', size], args, [thumbnailfile]]
		#subprocess.check_output(list(itertools.chain(*args)), stderr=subprocess.STDOUT)
		subprocess.check_output(['imagemagickconvert',  inputfile, '-resize', size, thumbnailfile], stderr=subprocess.STDOUT)

		if(os.path.getsize(thumbnailfile) == 0):
			raise Exception("File is empty.")

		# upload preview image
		url=host + 'api/fileThumbnail?key=' + playserverKey
		thumbnailUpload = open(thumbnailfile, 'rb')
		r = requests.post(url, files={"File" : thumbnailUpload})
		thumbnailUpload.close()
		r.raise_for_status()
		thumbnailid = r.json()['id']

		# associate uploaded thumbnail with original file/query
		#print get_image_data(previewfile)
		url=host + apipath + fileid + '/thumbnails/' + thumbnailid + '?key=' + playserverKey
		r = requests.post(url);
		r.raise_for_status()

		logger.debug("[%s] created thumbnail of type %s", fileid, ext)
	finally:
		try:
			os.remove(thumbnailfile)
		except:
			pass

def get_image_data(imagefile):
	global logger

	text=subprocess.check_output(['identify', imagefile], stderr=subprocess.STDOUT)
	return text

def on_message(channel, method, header, body):
	global logger
	statusreport = {}
	inputfile=None
	logger.debug("top of onmessage")
	try:
		#different paths for file vs query
		apipath = 'api/files/'
		if "query" in method.routing_key:
			print "\n received query"
			apipath='api/queries/'
		if "file" in method.routing_key:
			print "\n received file"

		# parse body back from json
		jbody=json.loads(body)

		host=jbody['host']
		fileid=jbody['id']
		intermediatefileid=jbody['intermediateId']
		if not (host.endswith('/')):
			host += '/'

		# for status reports
		statusreport['file_id'] = fileid
		statusreport['extractor_id'] = 'imageThumbnail'

		# print what we are doing
		logger.debug("[%s] started processing", fileid)

		# fetch data
		statusreport['status'] = 'Downloading image file.'
		statusreport['start'] = time.strftime('%Y-%m-%dT%H:%M:%S')
		channel.basic_publish(exchange='',
							routing_key=header.reply_to,
							properties=pika.BasicProperties(correlation_id = \
														header.correlation_id),
							body=json.dumps(statusreport))
		url=host + apipath + intermediatefileid + '?key=' + playserverKey
		r=requests.get(url, stream=True)
		r.raise_for_status()
		(fd, inputfile)=tempfile.mkstemp()
		with os.fdopen(fd, "w") as f:
			for chunk in r.iter_content(chunk_size=10*1024):
				f.write(chunk)

		# create thumbnail
		statusreport['status'] = 'Creating image thumbnail.'
		statusreport['start'] = time.strftime('%Y-%m-%dT%H:%M:%S')
		channel.basic_publish(exchange='',
							routing_key=header.reply_to,
							properties=pika.BasicProperties(correlation_id = \
														header.correlation_id),
							body=json.dumps(statusreport))
		create_image_thumbnail(inputfile, 'jpg', '120x120', host, apipath, fileid)
		#create_image_preview(inputfile, 'jpg', '800x600>', host, fileid, key, '-rotate', '90')
		#create_image_preview(inputfile, 'jpg', '800x600>', host, fileid, key, '-rotate', '180')
		#create_image_preview(inputfile, 'jpg', '800x600>', host, fileid, key, '-rotate', '270')

		# Ack
		channel.basic_ack(method.delivery_tag)
		logger.debug("[%s] finished processing", fileid)
	except subprocess.CalledProcessError as e:
		logger.exception("[%s] error processing [exit code=%d]\n%s", fileid, e.returncode, e.output)
		statusreport['status'] = 'Error processing.'
		statusreport['start'] = time.strftime('%Y-%m-%dT%H:%M:%S')
		channel.basic_publish(exchange='',
				routing_key=header.reply_to,
				properties=pika.BasicProperties(correlation_id = \
												header.correlation_id),
				body=json.dumps(statusreport))
		channel.basic_ack(method.delivery_tag)
	except Exception as e:
		logger.exception("[%s] error processing:\n%s", fileid, e.message)
		statusreport['status'] = 'Error processing.'
		statusreport['start'] = time.strftime('%Y-%m-%dT%H:%M:%S')
		channel.basic_publish(exchange='',
                routing_key=header.reply_to,
                properties=pika.BasicProperties(correlation_id = \
                                                header.correlation_id),
                body=json.dumps(statusreport))
		if('r' in vars()):
			if(r.status_code == 500 or r.status_code == 503):
				channel.basic_nack(method.delivery_tag, multiple=False, requeue=True)
			else:
				channel.basic_ack(method.delivery_tag)
		else:
			channel.basic_ack(method.delivery_tag)
	finally:
		statusreport['status'] = 'DONE.'
		statusreport['start'] = time.strftime('%Y-%m-%dT%H:%M:%S')
		channel.basic_publish(exchange='',
							routing_key=header.reply_to,
							properties=pika.BasicProperties(correlation_id = \
														header.correlation_id),
							body=json.dumps(statusreport))
		if inputfile is not None:
			try:
				os.remove(inputfile)
			except OSError:
				pass
			except UnboundLocalError:
				pass


if __name__ == "__main__":
	main()

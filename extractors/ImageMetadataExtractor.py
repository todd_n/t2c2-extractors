'''
Created on 14 Mar 2013

@author: Constantinos Sophocleous
'''

#!/usr/bin/env python
import pika
import sys
import logging
import json
import traceback
import requests
import tempfile
import subprocess
import os
import itertools
import re
import codecs
import time
from ssl import PROTOCOL_SSLv3
import dm3reader_v072
import sem_reader


def main():
    global logger

    # name of receiver
    receiver='ncsa.image'

    # configure the logging system
    logging.basicConfig(format="%(asctime)-15s %(name)-10s %(levelname)-7s : %(message)s", level=logging.WARN)
    logger = logging.getLogger(receiver)
    logger.setLevel(logging.DEBUG)

    if len(sys.argv) < 8:
        logger.info("Input RabbitMQ server address, followed by RabbitMQ server port,  RabbitMQ username, RabbitMQ password, Medici REST API key, RabbitMQ exchange name and whether SSL is to be used to communicate with the RabbitMQ messaging service (true of false).")
        logger.info("If SSL is used, add the path to the key certificate file and the path to the key file.")
        sys.exit()

    global playserverKey
    playserverKey = sys.argv[5]

    global exhangeName
    exhangeName = sys.argv[6]

#    if(sys.argv[1].endswith(os.sep)):
#        sys.argv[1] = (sys.argv[1])[:-1]

    # connect to rabbitmq using input username and password
    credentials = pika.PlainCredentials(sys.argv[3], sys.argv[4])
    host = sys.argv[1]
    port = int(sys.argv[2])

    useSsl = (sys.argv[7] == "true")
    if useSsl:
        sslCertPath = sys.argv[8]
        sslKeyPath = sys.argv[9]

        ssl_options = {"certfile": sslCertPath, "server_side": True, "keyfile": sslKeyPath, "ssl_version": PROTOCOL_SSLv3}

        parameters = pika.ConnectionParameters(credentials=credentials, ssl=useSsl, ssl_options=ssl_options, port=port, host=host)
    else:
        parameters = pika.ConnectionParameters(credentials=credentials, port=port, host=host)
    connection = pika.BlockingConnection(parameters)

    # connect to channel
    channel = connection.channel()

    # declare the exchange
    channel.exchange_declare(exchange=exhangeName, exchange_type='topic', durable=True)

    # declare the queue
    channel.queue_declare(queue=receiver, durable=True)

    # Set prefetch-count to 1 to avoid crashing due to interleaved frames and also send jobs to idle workers first
    channel.basic_qos(prefetch_count=1)

    # connect queue and exchange
    channel.queue_bind(queue=receiver, exchange=exhangeName, routing_key='*.file.image.#')

    # create listener
    channel.basic_consume(on_message, queue=receiver, no_ack=False, consumer_tag="medici_image_metadata_identify")

    # start listening
    logger.info("Waiting for messages. To exit press CTRL+C")
    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        channel.stop_consuming()

    # close connection
    connection.close()

def create_dm3_metadata(fileid,host,fpath,tags,*args):
    jsonArray = json.dumps(tags)

    jsonArray = jsonArray.replace('.','_')


    logger.debug(jsonArray)

    headers={'Content-Type': 'application/json'}
    url=host + 'api/files/' + fileid + '/metadata' + '?key=' + playserverKey
    r = requests.post(url, headers=headers, data=jsonArray);
    r.raise_for_status()

    logger.debug("[%s] created metadata.", fileid)

def create_image_metadata(fileid, host, fpath, *args):
    global logger


#    tempf=open("temp_" + fileid, "w")
#    pipe = subprocess.Popen([sys.argv[1] + os.sep +'identify', '-quiet', '-verbose', fileid] ,stderr=subprocess.STDOUT, stdout=tempf)
#    pipe.wait()
#    tempf.flush()
#    tempf.close()
#    tempf=open("temp_" + fileid, "r")
#    unparsedMetadata = tempf.read()
#    tempf.close()
#    os.remove("temp_" + fileid)

    tempfd,tempfpath =tempfile.mkstemp(text =True)
    tempf = open(tempfpath, 'w')
    #sys.argv[1] + os.sep +
    pipe = subprocess.Popen(['identify', '-quiet', '-verbose', fpath] ,stderr=subprocess.STDOUT, stdout=tempf)
    pipe.wait()
    tempf.flush()
    tempf.close()
    tempf = open(tempfpath, 'r')
    unparsedMetadata = tempf.read()
    tempf.close()
    os.close(tempfd)
    try:
        os.remove(tempfpath)
    except OSError:
        pass

    logger.debug(unparsedMetadata)

    #Parser
    numberOfImages = 0

    imgMetadata="{"
    level = 1
    isFirstLine = True

    inHistogramColormap = False
    histogramColormapFirst = False
    histogramColormapFirstPassed = False

    unparsedMetadataList = unparsedMetadata.splitlines()
    for unparsedMetadataLine in unparsedMetadataList:
        if unparsedMetadataLine.rstrip().lstrip().lower().startswith("colormap:"):
            unparsedMetadataLine = re.sub(r'colormap: [0-9]+', r'colormap:', unparsedMetadataLine)
        if unparsedMetadataLine.rstrip().lstrip().lower().startswith("histogram:"):
            unparsedMetadataLine = re.sub(r'histogram: [0-9]+', r'histogram:', unparsedMetadataLine)

        if (unparsedMetadataLine.rstrip().lstrip() == "" or (not ":" in unparsedMetadataLine) ) :
            continue

        unparsedMetadataLine = re.sub(r'^\$', '', unparsedMetadataLine)
        endOfFieldName = unparsedMetadataLine.find(": ")
        if endOfFieldName == -1:
            unparsedMetadataLine = re.sub(r'\.', r'-', unparsedMetadataLine)
        else:
            unparsedMetadataLine = re.sub(r'\.', r'-', unparsedMetadataLine[0:endOfFieldName]) + unparsedMetadataLine[endOfFieldName:]

        if unparsedMetadataLine.startswith('Image: '):
            if numberOfImages > 0:
                unparsedMetadataLine = unparsedMetadataLine.replace('Image: ', 'Tagged image ' + str(numberOfImages) + ': ')
            numberOfImages = numberOfImages + 1

        if numberOfImages > 1:
            unparsedMetadataLine = "  " + unparsedMetadataLine

        if not isFirstLine:
            while(True):
                if (unparsedMetadataLine.rstrip().lstrip().lower().startswith("histogram:") or unparsedMetadataLine.rstrip().lstrip().lower().startswith("colormap:")):
                    inHistogramColormap = True
                elif (inHistogramColormap and not histogramColormapFirst and not histogramColormapFirstPassed):
                    histogramColormapFirst = True

                numOfSpaces = len(unparsedMetadataLine) - len(unparsedMetadataLine.lstrip(' '))
                theKey = unparsedMetadataLine.lstrip()[0:unparsedMetadataLine.lstrip().find(":")]
                if histogramColormapFirst:
                    histogramColormapFirst = False
                    histogramColormapFirstPassed = True
                    imgMetadata = imgMetadata + "{"
                elif (inHistogramColormap and histogramColormapFirstPassed and theKey.isdigit()):
                    imgMetadata = imgMetadata + ","
                elif (inHistogramColormap and histogramColormapFirstPassed and not theKey.isdigit()):
                    inHistogramColormap = False
                    histogramColormapFirst = False
                    histogramColormapFirstPassed = False
                    imgMetadata = imgMetadata + "}"
                    continue
                elif numOfSpaces == level * 2:
                    imgMetadata = imgMetadata + ","
                elif numOfSpaces > level * 2:
                    imgMetadata = imgMetadata + "{"
                    level = level + 1
                elif numOfSpaces < level * 2:
                    for j in range(numOfSpaces, level * 2, 2):
                        imgMetadata = imgMetadata + "}"
                    imgMetadata = imgMetadata + ","
                    level = level - (level * 2 - numOfSpaces) / 2

                break
        else:
            isFirstLine = False

        unparsedMetadataLine = ("\"" + unparsedMetadataLine.lstrip().replace(": ","\": \"") + "\"").replace(":\"","\":")
        imgMetadata = imgMetadata + unparsedMetadataLine

    for i in range(level):
        imgMetadata = imgMetadata + "}"

    imgMetadata = re.sub(r'"([^"]+)"\{', r'{"": "\1",', imgMetadata)
    imgMetadata = re.sub(r'"Image": "([^"]+)"', '"Image": "' + fileid +'"', imgMetadata)
    imgMetadata = re.sub(r'"filename": "([^"]+)"', '"filename": "' + fileid +'"', imgMetadata)
    imgMetadata = re.sub(r'[ \t]+"', '"', imgMetadata)
    imgMetadata = re.sub('"[ \t]*":"[^"]*",', '', imgMetadata)
    #End of parser



    logger.debug(imgMetadata)

    headers={'Content-Type': 'application/json'}
    url=host + 'api/files/' + fileid + '/metadata' + '?key=' + playserverKey
    r = requests.post(url, headers=headers, data=imgMetadata);
    r.raise_for_status()

    logger.debug("[%s] created metadata.", fileid)


def on_message(channel, method, header, body):
    global logger
    statusreport = {}
    try:
        # parse body back from json
        jbody=json.loads(body)

        if not 'wasRaw' in jbody['flags']:
            host=jbody['host']
            fileid=jbody['id']
            intermediatefileid=jbody['intermediateId']

            # for status reports
            statusreport['file_id'] = fileid
            statusreport['extractor_id'] = 'ImageMetadataExtractorIdentify'

            # print what we are doing
            logger.debug("[%s] started processing", fileid)

            # fetch data
            statusreport['status'] = 'Downloading image file.'
            statusreport['start'] = time.strftime('%Y-%m-%dT%H:%M:%S')
            channel.basic_publish(exchange='',
                         routing_key=header.reply_to,
                         properties=pika.BasicProperties(correlation_id = \
                                                         header.correlation_id),
                         body=json.dumps(statusreport))
            url=host + 'api/files/' + intermediatefileid + '?key=' + playserverKey
            r=requests.get(url, stream=True)
            r.raise_for_status()

            fd,fpath = tempfile.mkstemp()
            f = open(fpath, "wb")
            for chunk in r.iter_content(chunk_size=10*1024):
                f.write(chunk)
            f.close()
            os.close(fd)
            tags = []
            pgm_file = None
            """
            the code below extracts the metadata tags and the pgm_file, a greyscale image of the .dm3 file
            """


            fileName = jbody['filename']
            """
            metadata extractor for sem .tiff files not yet implemented
            """
            if ".tif" in fileName:
                try:
                    metadataFile = sem_reader.extract_sem_metadata(fpath)
                    metadataJson = json.loads(metadataFile)
                except:
                    pass

            if ".dm3" in fileName:
                try:
                    #tags = dm3reader_v072.parseDM3(fpath)
                    tags = dm3reader_v072.extract_dm3_metadata(fpath,dump=False)
                    #dm3reader_v072.getDM3FileInfo(fpath, makePGMtn=True, tn_file='dm3tn_temp.pgm' )
                    #pgm_file = dm3reader_v072.make_pgm(fpath,tn_file='dm3tn_temp.pgm',**tags)
                except:
                    pass


            # create metadata
            statusreport['status'] = 'Extracting metadata.'
            statusreport['start'] = time.strftime('%Y-%m-%dT%H:%M:%S')
            channel.basic_publish(exchange='',
                         routing_key=header.reply_to,
                         properties=pika.BasicProperties(correlation_id = \
                                                         header.correlation_id),
                         body=json.dumps(statusreport))
            try:
                if type(tags) == dict:
                    create_dm3_metadata(fileid,host,fpath,tags,None)
                else:
                    pass
            except:
                logger.info("Failed to upload dm3 metadata")
            try:
                create_image_metadata(fileid, host, fpath)
            except:
                logger.info("create image metadata did not work")

            logger.debug("[%s] finished processing", fileid)
        else:
            logger.info("Image metadata to be extracted from raw image. Aborting processing.");

        # Ack
        channel.basic_ack(method.delivery_tag)

    except subprocess.CalledProcessError as e:
        logger.exception("[%s] error processing [exit code=%d]\n%s", fileid, e.returncode, e.output)
        statusreport['status'] = 'Error processing.'
        statusreport['start'] = time.strftime('%Y-%m-%dT%H:%M:%S')
        channel.basic_publish(exchange='',
                routing_key=header.reply_to,
                properties=pika.BasicProperties(correlation_id = \
                                                header.correlation_id),
                body=json.dumps(statusreport))
        channel.basic_ack(method.delivery_tag)
    except Exception as e:
        logger.exception("[%s] error processing:\n%s", fileid, e.message)
        statusreport['status'] = 'Error processing.'
        statusreport['start'] = time.strftime('%Y-%m-%dT%H:%M:%S')
        channel.basic_publish(exchange='',
                routing_key=header.reply_to,
                properties=pika.BasicProperties(correlation_id = \
                                                header.correlation_id),
                body=json.dumps(statusreport))
        if('r' in vars()):
            if(r.status_code == 500 or r.status_code == 503):
                channel.basic_nack(method.delivery_tag, multiple=False, requeue=True)
            else:
                channel.basic_ack(method.delivery_tag)
        else:
            channel.basic_ack(method.delivery_tag)
    finally:
        statusreport['status'] = 'DONE.'
        statusreport['start'] = time.strftime('%Y-%m-%dT%H:%M:%S')
        channel.basic_publish(exchange='',
                         routing_key=header.reply_to,
                         properties=pika.BasicProperties(correlation_id = \
                                                         header.correlation_id),
                         body=json.dumps(statusreport))
        try:
            os.remove(fpath)
        except OSError:
            pass
        except UnboundLocalError:
            pass



if __name__ == "__main__":
    main()


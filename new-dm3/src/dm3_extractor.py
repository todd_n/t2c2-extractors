import json
import logging
import os
import tempfile
import dm3reader_v072
import extractor_config
import subprocess
import re
from extractor_config import *
import pyclowder.extractors as extractors

def main():
    global extractorName, messageType, rabbitmqExchange, rabbitmqURL, registrationEndpoints,logger
    global imageBinary, imageType, imageThumbnail, imagePreview
    global previewBinary, previewType, previewCommand

    receiver = 'new.dm3.extractor'
    extractorName = extractor_config.extractorName
    messageType = extractor_config.messageType
    rabbitmqExchange = extractor_config.rabbitmqExchange
    rabbitmqURL = extractor_config.rabbitmqURL
    playserverKey = extractor_config.playserverKey


    logging.basicConfig(format='%(levelname)-7s : %(name)s -  %(message)s', level=logging.WARN)
    logging.getLogger('pyclowder.extractors').setLevel(logging.INFO)

    logger = logging.getLogger(receiver)
    logger.setLevel(logging.DEBUG)

    extractors.connect_message_bus(extractorName=extractorName, messageType=messageType, processFileFunction=processFile,
        checkMessageFunction=check_message, rabbitmqExchange=rabbitmqExchange, rabbitmqURL=rabbitmqURL)

def check_message(parameters):
    return True

def processFile(parameters):
    input_file = parameters['inputfile']
    file_name = str(parameters['filename'])
    if file_name.endswith('.dm3'):
        try:
            tags = dm3reader_v072.extract_dm3_metadata(input_file, dump=False)
            top_tags = dm3reader_v072.get_metadata_shortlist(**tags)
            all_metadata = dict()
            all_metadata["top_metadata"] = top_tags
            all_metadata["all_metadata"] = tags
            extractors.upload_file_metadata(all_metadata, parameters)
        except:
            print('error')
        if imagePreview:
            execute_command(parameters, imageBinary, imagePreview, imageType, False)



    print('found input file')

def execute_command(parameters, binary, commandline, ext, thumbnail=False):
    global logger

    (fd, tmpfile)=tempfile.mkstemp(suffix='.' + ext)
    try:
        # close tempfile
        os.close(fd)
        file_name = None
        file_name = parameters['filename']
        if ".dm3" in file_name:
            input_file = dm3reader_v072.make_pgm_tempfile(parameters['inputfile'])
            commandline = commandline.replace('@BINARY@', binary)
            commandline = commandline.replace('@INPUT@', input_file)
            commandline = commandline.replace('@OUTPUT@', tmpfile)
            # split command line
            p = re.compile(r'''((?:[^ "']|"[^"]*"|'[^']*')+)''')
            commandline = p.split(commandline)[1::2]

            # execute command
            x = subprocess.check_output(commandline, stderr=subprocess.STDOUT)
            if x:
                logger.debug(binary + " : " + x)

            if (os.path.getsize(tmpfile) != 0):
                extractors.upload_preview(previewfile=tmpfile, parameters=parameters)

    except subprocess.CalledProcessError as e:
        logger.error(binary + " : " + str(e.output))
        raise
    finally:
      try:
        os.remove(tmpfile)
      except:
        pass


if __name__ == "__main__":
    main()




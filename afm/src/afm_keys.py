import json
import os
import numpy

folder = "/Users/todd_n/Desktop/test/"

contents = os.listdir(folder)

def get_keys_from_file(filepath):
    f = open(filepath,'r')
    json_str = f.read()
    json_data = json.loads(json_str)[0]
    json_content = json_data['content']
    current_keys = json_content.keys()
    f.close()
    return current_keys

keys = []

for each in contents:
    if os.path.isdir(folder+each):
        contents_inside = os.listdir(folder+each)
        if ('_metadata.json' in contents_inside):
            current_path = folder+each+'/_metadata.json'
            current_keys = get_keys_from_file(current_path)
            keys.append(current_keys)


print('done')
a = set(keys[0])
b = set(keys[1])
c = set(keys[2])
d = set(keys[3])

total = a | b | c | d

common = a & b & c & d

total = list(total)
total.sort()

common = list(common)
common.sort()

f = open('/Users/todd_n/Desktop/afm_keys.txt','wb')
for each in total:
    f.write(each+'\n')
f.close()

f = open('/Users/todd_n/Desktop/afm_keys_common.txt','wb')
for each in common:
    f.write(each+'\n')
f.close()






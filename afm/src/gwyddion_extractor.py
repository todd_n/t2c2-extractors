import gwy
import tempfile
import sys
import json

test_afm_file = "/Users/todd_n/Desktop/RE__AFM_X-ray/AFM_GLAM0000.ibw"



other_test_file = "/Users/todd_n/Desktop/RE__AFM_X-ray/STM_0001.ibw"

broken_test = '/Users/todd_n/Desktop/RE__AFM_X-ray/Turquoise_0004.ibw'

mrl_folder = "/Users/todd_n/Desktop/RE__AFM_X-ray"


def create_image_previews(fpath):
    num_previews = len(gwy.gwy_app_data_browser_get_data_ids(fpath))
    container = gwy.gwy_file_load(test_afm_file, gwy.RUN_NONINTERACTIVE)


def create_image_preview(container, current_channel):
    data_field_name = gwy.gwy_app_get_data_field_title(container,current_channel)
    field_thumbnail = gwy.gwy_app_get_channel_thumbnail(container,current_channel,1000,1000)
    (fd, tn_file) = tempfile.mkstemp(suffix=".png")
    field_thumbnail.save(tn_file,'png')

    return tn_file

def create_image_preview_from_file(container, current_channel):
    #container = gwy.gwy_file_load(test_afm_file, gwy.RUN_NONINTERACTIVE)
    gwy.gwy_app_data_browser_add(container)
    gwy.gwy_app_data_browser_select_data_field(container, current_channel)
    (fd, tn_file) = tempfile.mkstemp(suffix=".png")
    gwy.gwy_file_save(container, tn_file, gwy.RUN_NONINTERACTIVE)

    return tn_file


#tfile = create_image_preview_from_file(test_afm_file,0)



#f = open(test_afm_file,'r')

def getMetadata(container):
    meta = container['/0/meta']
    metadata_dict = dict()
    try:
        meta_keys = meta.keys()
        meta_key_names = meta.keys_by_name()
        for i in range(0, len(meta_keys)):
            each = meta_keys[i]
            try:
                contains = meta.contains(each)
                value_type = meta.value_type(each)
                try:
                    t = meta.get_string(each)
                    new_key = (meta_key_names[i].replace('.','_')).encode('utf-8').strip()
                    new_value = t.encode('utf-8').strip()
                    metadata_dict[new_key] = new_value
                except:
                    try:
                        t = meta.get_double(each)
                        #metadata_dict[meta_key_names[i].replace(',',' ')] = t
                    except:
                        pass
            except:
                pass
    except:
        pass
    return metadata_dict

def getDataFieldName(container,channel):
    dfield_title = container['/'+str(channel)+'/data/title']
    dfield = dict()
    dfield['title'] = dfield_title.encode('utf-8').strip()

    return dfield_title


""""
container = gwy.gwy_file_load(test_afm_file, gwy.RUN_NONINTERACTIVE)
gwy.gwy_app_data_browser_add(container)

gwy.gwy_app_data_browser_select_data_field(container, 0)
ids = gwy.gwy_app_data_browser_get_data_ids(container)



old = []
for each in ids:
    current = []
    current = str(each)
    dfield = container['/'+str(each)+'/data']
    dfield_title = container['/'+str(each)+'/data/title']
    log = container['/'+str(each)+'/data/log']
    for i in range(0,log.get_length()):
        try:
            current = log.get(i)
        except:
            pass
    meta = container['/'+str(each)+'/meta']
    print("doing datafield : " + dfield_title)
    value_types = []
    dfield_values = []
    try:
        meta_keys = meta.keys()
        meta_key_names = meta.keys_by_name()
        for i in range(0,len(meta_keys)):
            each = meta_keys[i]
            try:
                contains = meta.contains(each)
                value_type = meta.value_type(each)
                value_types.append(value_type)
                try:
                    t = meta.get_string(each)
                    print(meta_key_names[i]+ " : " + t)
                    dfield_values.append(meta_key_names[i]+ " : " + t)
                except:
                    try:
                        t = meta.get_double(each)
                        print(meta_key_names[i] + " : " + t)
                    except:
                        print("error")
            except:
                pass
    except:
        pass
    value_types = set(value_types)
    value_types = list(value_types)
    print("Done doing datafield : " + dfield_title)
    old.append(dfield_values)

same = (old[0] == old[1])
gwy.gwy_file_save(container, 'whatever.png', gwy.RUN_NONINTERACTIVE)
gwy.gwy_app_data_browser_remove(container)



print('done')
"""""
#f.close()

#!/usr/bin/env python

import logging
import os
import subprocess
import requests
import json
import tempfile
import re
import gwy
import pyclowder.extractors as extractors
import gwyddion_extractor
from gwyddion_preview_config_withkey import *

"""

to install pyclowder :

pip install git+https://opensource.ncsa.illinois.edu/stash/scm/cats/pyclowder.git

"""



def main():
    global extractorName, messageType, rabbitmqExchange, rabbitmqURL, logger

    #set logging
    logging.basicConfig(format='%(levelname)-7s : %(name)s -  %(message)s', level=logging.INFO)
    logging.getLogger('pymedici.extractors').setLevel(logging.DEBUG)
    logger = logging.getLogger(extractorName)
    logger.setLevel(logging.DEBUG)

    #connect to rabbitmq
    extractors.connect_message_bus(extractorName=extractorName,
                                   messageType=messageType,
                                   processFileFunction=process_file,
                                   rabbitmqExchange=rabbitmqExchange,
                                   rabbitmqURL=rabbitmqURL)


def check_message(parameters):
    return True
# ----------------------------------------------------------------------
# Process the file and upload the results
def process_file(parameters):
    global imageBinary, imageType, imageThumbnail, imagePreview
    global previewBinary, previewType, previewCommand

    print(parameters['inputfile'])

    if imageBinary:
        execute_command(parameters, imageBinary, imageThumbnail, imageType, True)
        #execute_command(parameters, imageBinary, imagePreview, imageType, False)
    if previewBinary:
        execute_command(parameters, previewBinary, previewCommand, previewType, False)

def execute_command(parameters, binary, commandline, ext, thumbnail=True):
    global logger

    (fd, tmpfile)=tempfile.mkstemp(suffix='.' + ext)
    try:
        # close tempfile
        os.close(fd)
        file_name = None
        try:
            file_name = parameters['filename']
        except:
            file_name = parameters['inputfile']
        if ".ibw" in file_name:
            container = gwy.gwy_file_load(parameters['inputfile'], gwy.RUN_NONINTERACTIVE)
            gwy.gwy_app_data_browser_add(container)
            try:
                optional_parameters = parameters['parameters']
                asJson = json.loads(optional_parameters)
                template_name = asJson["templateName"]
                template_keys = asJson["templateKeys"]
            except:
                logger.info("No optional parameters")

            total_metadata = dict()
            file_metadata = dict()
            template_metadata = dict()
            try:
                meta = container['/0/meta']
                try:
                    meta_keys = meta.keys()
                    meta_key_names = meta.keys_by_name()#this has the names
                    for i in range(0, len(meta_keys)):
                        each = meta_keys[i]
                        try:
                            contains = meta.contains(each)
                            value_type = meta.value_type(each)
                            try:
                                t = meta.get_string(each)
                                new_key = (meta_key_names[i].replace('.', '_')).encode('utf-8').strip()
                                new_value = t.encode('utf-8').strip()
                                if (meta_key_names[i] in template_keys):
                                    template_metadata[new_key] = new_value
                                file_metadata[new_key] = new_value
                            except:
                                try:
                                    t = meta.get_double(each)
                                    # metadata_dict[meta_key_names[i].replace(',',' ')] = t
                                except:
                                    pass
                        except:
                            pass
                except:
                    pass
            except:
                pass
            #TODO use keys that were input here
            total_metadata["all metadata"] = file_metadata
            total_metadata[template_name + " template"] = template_metadata
            #fake_metadata = generate_fake_metadata()
            #fake_metadata_json = json.dumps(fake_metadata)
            extractors.upload_file_metadata(total_metadata,parameters)

    except subprocess.CalledProcessError as e:
        logger.error(str(e.output))
        raise
    finally:
      try:
        os.remove(tmpfile)
      except:
        pass



def upload_preview_title(previewid, title_name,parameters):
    key = parameters['secretKey']
    title = dict()
    title['title'] = title_name
    host = parameters['host']
    if (not host.endswith("/")):
        host = host + "/"
    url = host + 'api/previews/'+previewid+'/title?key='+key

    headers = {'Content-Type': 'application/json'}

    if url:
        r = requests.post(url, headers=headers, data=json.dumps(title), verify=sslVerify);
        r.raise_for_status()
    return r

def generate_fake_metadata():
    md = dict()
    nested_md = dict()
    nested_md["billy"] = "budd"
    nested_md["jane"] = "eyre"
    nested_md["don"] = "corleone"
    md["above"] = "below"
    md["sky"] = "land"
    md["characters"] = nested_md
    return md


if __name__ == "__main__":
    main()

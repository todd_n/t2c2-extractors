#!/usr/bin/env python

import logging
import os
import subprocess
import requests
import json
import tempfile
import pika
import time
import re
import gwy
import pyclowder.extractors as extractors
import gwyddion_extractor
import gwyddion_preview_config
from gwyddion_preview_config import *

"""

to install pyclowder :

pip install git+https://opensource.ncsa.illinois.edu/stash/scm/cats/pyclowder.git

"""



def main():
    global extractorName, messageType, rabbitmqExchange, rabbitmqURL, logger

    #set logging
    logging.basicConfig(format='%(levelname)-7s : %(name)s -  %(message)s', level=logging.INFO)
    logging.getLogger('pymedici.extractors').setLevel(logging.DEBUG)
    logger = logging.getLogger(extractorName)
    logger.setLevel(logging.DEBUG)
    print rabbitmqURL

    n_retries = 0
    while(True):
        try:
            #connect to rabbitmq
            extractors.connect_message_bus(
                    extractorName=extractorName, 
                    messageType=messageType, 
                    processFileFunction=process_file, 
                    rabbitmqExchange=rabbitmqExchange, 
                    rabbitmqURL=rabbitmqURL)
            break
        except pika.exceptions.AMQPConnectionError:
            n_retries += 1
            if n_retries == 5:
                break
            print 'Cannot connect to RabbitMQ server. Retry in 5s...'
            time.sleep(5)


def check_message(parameters):
    print("checking message")
    print(parameters['host'])
    return True
# ----------------------------------------------------------------------
# Process the file and upload the results
def process_file(parameters):
    global imageBinary, imageType, imageThumbnail, imagePreview
    global previewBinary, previewType, previewCommand

    print(parameters['inputfile'])

    if imageBinary:
        execute_command(parameters, imageBinary, imageThumbnail, imageType, True)
        #execute_command(parameters, imageBinary, imagePreview, imageType, False)
    if previewBinary:
        execute_command(parameters, previewBinary, previewCommand, previewType, False)

def execute_command(parameters, binary, commandline, ext, thumbnail=True):
    global logger

    (fd, tmpfile)=tempfile.mkstemp(suffix='.' + ext)
    try:
        # close tempfile
        os.close(fd)
        file_name = None
        try:
            file_name = parameters['filename']
        except:
            file_name = parameters['inputfile']
        if ".ibw" in file_name:
            container = gwy.gwy_file_load(parameters['inputfile'], gwy.RUN_NONINTERACTIVE)
            gwy.gwy_app_data_browser_add(container)
            file_metadata = dict()
            try:
                meta = container['/0/meta']
                try:
                    meta_keys = meta.keys()
                    meta_key_names = meta.keys_by_name()
                    for i in range(0, len(meta_keys)):
                        each = meta_keys[i]
                        try:
                            contains = meta.contains(each)
                            value_type = meta.value_type(each)
                            try:
                                t = meta.get_string(each)
                                new_key = (meta_key_names[i].replace('.', '_')).encode('utf-8').strip()
                                new_value = t.encode('utf-8').strip()
                                file_metadata[new_key] = new_value
                            except:
                                try:
                                    t = meta.get_double(each)
                                    # metadata_dict[meta_key_names[i].replace(',',' ')] = t
                                except:
                                    pass
                        except:
                            pass
                except:
                    pass
            except:
                pass
            extractors.upload_file_metadata(file_metadata,parameters)
            #f = open(parameters['filename']+"file_metadata.txt","wb")
            #mdkeys = file_metadata.keys()
            #for each in mdkeys:
            #    currentLine = each+" : "+file_metadata[each]
            #    f.write(currentLine+"\n")
            #f.close()
            #try:
            #    os.remove(parameters['filename']+"file_metadata.txt")
            #except:
             #   print("error deleting file")
            #metadata_file_id = extractors.upload_file_to_dataset(parameters['filename']+"file_metadata.txt",parameters)
            #change author here

            try:
                author = get_file_author(parameters['fileid'])
                #result = change_file_author(metadata_file_id,author)
            except:
                pass
            input_file = None
            ids = gwy.gwy_app_data_browser_get_data_ids(container)
            for i in range(0,(len(ids))):
                try:
                    input_file = gwyddion_extractor.create_image_preview_from_file(container,i)
                    try:
                        dfield = container['/'+str(i)+'/data/title']
                        previewid = extractors.upload_preview(input_file,parameters,None)
                        upload_preview_title(previewid,dfield,parameters)
                        if ('heightretrace' in dfield.lower()):
                            extractors.upload_thumbnail(input_file,parameters)
                    except:
                        pass
                except:
                    pass
                try:
                    os.remove(input_file)
                except:
                    pass
            try :
                gwy.gwy_app_data_browser_remove(container)
            except:
                pass

    except subprocess.CalledProcessError as e:
        logger.error(str(e.output))
        raise
    finally:
      try:
        os.remove(tmpfile)
      except:
        pass

def upload_preview_title(previewid, title_name,parameters):
    key = parameters['secretKey']
    title = dict()
    title['title'] = title_name
    host = parameters['host']
    if (not host.endswith("/")):
        host = host + "/"
    url = host + 'api/previews/'+previewid+'/title?key='+key

    headers = {'Content-Type': 'application/json'}

    if url:
        r = requests.post(url, headers=headers, data=json.dumps(title), verify=sslVerify);
        r.raise_for_status()
    return r

def get_file_author(f_id):
    clowder_url = 'http://' + gwyddion_preview_config.clowderhost + ':' + gwyddion_preview_config.clowderport + '/'
    secret_key = gwyddion_preview_config.playserverKey
    requestURL = clowder_url + 'api/files/'+f_id+'/getOwner?key='+secret_key
    r = requests.get(requestURL)
    asJson = r.json()
    return asJson

def change_file_author(f_id,new_author_id):

    clowder_url = 'http://' + gwyddion_preview_config.clowderhost + ':' + gwyddion_preview_config.clowderport + '/'
    secret_key = gwyddion_preview_config.playserverKey
    requestURL = clowder_url + 'api/collections/' + f_id + '/changeOwnerFile/' + new_author_id + "?key=" + secret_key
    # headers = {'Content-type': 'application/json'}
    # body = {'rootId': col_id, 'newUserId': new_author_id, 'methodKey': zip_config.key_4ceed}
    r = requests.post(requestURL)
    return r

if __name__ == "__main__":
    main()

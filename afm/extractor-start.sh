#!/bin/bash

# add/replace if variable is non empty
# $1 = variable to replace/remove
# $2 = new value to set
function update_conf() {
    local query
    if [ "$1" == "" ]; then return 0; fi

    # First remove existing configuration info
    if [ -e /code/gwyddion_preview_config.py ]; then
        if [ "$2" != "" ]; then
            query="$1"
	    mv /code/gwyddion_preview_config.py /code/gwyddion_preview_config.py.old
            grep -v "^$query" /code/gwyddion_preview_config.py.old > /code/gwyddion_preview_config.py
            rm /code/gwyddion_preview_config.py.old
	fi
    fi

    # Then, update config info
    if [ "$2" != "" ]; then
        echo "$1=\"$2\"" >> /code/gwyddion_preview_config.py
    fi
}

# Set configuration information 
update_conf   "rabbitmqURL" "$RABBITMQ_URL"
# update_conf   "clowderhost" "$CLOWDER_URL"
# update_conf   "clowderport" "$CLOWDER_PORT"
# update_conf   "credentials_name" "$CRED_NAME"
# update_conf   "credentials_pass" "$CRED_PASS"
# update_conf   "playserverKey" "$SERVER_KEY"
# update_conf   "rabbitmqExchange" "$EXCHANGE_NAME"
# update_conf   "sslVerify" $USE_SSL
# update_conf   "messageType" "$MESSAGE_TYPE"

# Start clowder
cd /code
Xvfb :10 -ac -screen 0 1024x768x24 &
export DISPLAY=:10 
echo display
python gwyddion.image.preview.py

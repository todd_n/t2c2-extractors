import zipfile
import tempfile
import os
import shutil

"""
import tempfile
import shutil

dirpath = tempfile.mkdtemp()
# ... do stuff with dirpath
shutil.rmtree(dirpath)
"""

testFolder = "/Users/todd_n/Desktop/Pole figures"
test = "/Users/todd_n/Desktop/XRDData"

def unzipFileToTempDirectory(pathToZipFile):
    fd = tempfile.mkdtemp()

    zf = zipfile.ZipFile(pathToZipFile,'r')
    zf.extractall(fd)
    zf.close()
    return fd



def create_collection_from_zip(parameters):
    #logger.info('processing zip file')
    #create temp file

    #write zipfile to temp file

    #iterate over all contents of zipfile
    return 0

def postCollection(folder, parentId):
    print('posting collection with parent',parentId)

def postDataset(folder,parentId):
    print('posting dataset with parent', parentId)

def postFileToDataset(file,datasetId):
    print('posting file',file,'to dataset',datasetId)

def processFolder(folder, parentId,parameters):
    contents = os.listdir(folder)
    folderName = folder[folder.rfind('/')+1:]
    files_inside = []
    folders_inside = []
    for item in contents:
        try:
            subcontents = os.listdir(folder+'/'+item)
            folders_inside.append(folder+'/'+item)
        except:
            if (item != '.DS_Store'):
                files_inside.append(item)

    #collection case
    if ((len(files_inside) == 0) and (len(folders_inside) > 0)):
        collectionId = postCollection(folder,parentId)
        for each in folders_inside:
            processFolder(each,parentId,parameters)
    #dataset case
    elif ((len(files_inside) > 0) and (len(folders_inside) == 0)):
        datasetId = postDataset(folder,parentId)
        for each in files_inside:
            postFileToDataset(each,datasetId)
    elif ((len(files_inside) > 0) and (len(folders_inside) > 0)):
        collectionId = postCollection(folder, parentId)
        for each in folders_inside:
            processFolder(each,collectionId,parameters)

        datasetId = postDataset(folder,parentId)
        for each in files_inside:
            postFileToDataset(file,datasetId)

temp = unzipFileToTempDirectory('/Users/todd_n/Desktop/Pole figures.zip')
contents = os.listdir(temp)
thestuff = os.listdir(temp + '/' + contents[1])
shutil.rmtree(temp)

processFolder(test,"","")
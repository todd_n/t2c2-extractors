import pyclowder.api as pyclowderapi
import sys
import pika
import json
import logging
import zipfile
import tempfile
import pyclowder.extractors as extractors
import zip_extractor_config
import requests
import os

def main():
    global extractorName, messageType, rabbitmqExchange, rabbitmqURL, registrationEndpoints,logger

    receiver = 'zip.extrator'
    #set logging
    extractorName = 'zip.extractor'
    messageType = zip_extractor_config.messageType
    rabbitmqExchange = zip_extractor_config.exchange_name
    rabbitmqURL = "amqp://guest:guest@" + zip_extractor_config.rabbitmqhost + ":5672/%2f"
    host = zip_extractor_config.clowderhost
    port = zip_extractor_config.clowderport
    playserverKey = zip_extractor_config.playserverKey

    logging.basicConfig(format='%(levelname)-7s : %(name)s -  %(message)s', level=logging.WARN)
    logging.getLogger('pyclowder.extractors').setLevel(logging.INFO)

    logger = logging.getLogger(receiver)
    logger.setLevel(logging.DEBUG)

    extractors.connect_message_bus(extractorName=extractorName, messageType=extractorName, processFileFunction=create_collection_from_zip,
        checkMessageFunction=check_message, rabbitmqExchange=rabbitmqExchange, rabbitmqURL=rabbitmqURL)


def check_message(parameters):
    logger.info('checking message')
    return True

def unzipFileToTempDirectory(pathToZipFile):
    fd = tempfile.mkdtemp()

    zf = zipfile.ZipFile(pathToZipFile,'r')
    zf.extractall(fd)
    zf.close()
    return fd


def changeCollectionAuthor(parameters,rootId, newAuthorId):
    clowder_url = 'http://' + zip_extractor_config.clowderhost + ':' + zip_extractor_config.clowderport + '/'
    secret_key = zip_extractor_config.playserverKey
    requestURL = clowder_url+'api/collections/'+rootId+'/changeOwner/'+newAuthorId+"?key="+secret_key
    headers = {'Content-type': 'application/json'}
    body = {'rootId': rootId, 'newUserId': newAuthorId, 'methodKey': zip_extractor_config.key_4ceed}
    r = requests.post(requestURL,data=json.dumps(body),headers=headers)

    if (r.status_code != 200):
        logger.error('Problem changing owner of collection  : [%d] - %s)' %
              (r.status_code, r.text))
    #response = r['id']


def create_collection_from_zip(parameters):


    logger.info('processing zip file')
    logger.info("check the paramters")
    theZip = parameters['inputfile']
    temp = unzipFileToTempDirectory(theZip)
    all_contents = os.listdir(temp)
    theFolder = all_contents[1]
    currentRoot = ""
    return 0
    currentRoot = processFolder(temp+'/'+theFolder,"",parameters,theZip)

    optional_parameters = ""
    try:
        optional_parameters = parameters['parameters']
    except:
        logger.info('no optional parameters found, or error reading them')

    newOwner = ""
    if (optional_parameters != ""):
        try:
            content = json.loads(optional_parameters)
            if ('loggedInUser' in content):
                newOwner = content['loggedInUser']
        except:
            pass
    if (newOwner != ""):
        logger.info("new owner supplied")

    #changeCollectionAuthor(parameters, currentRoot, str(newOwner))
    changeCollectionAuthor(parameters,currentRoot, str(newOwner))

    #create temp file

    #write zipfile to temp file

    #iterate over all contents of zipfile

def postCollection(folder, parentId):
    folderName = folder[folder.rfind('/') + 1:]
    #print('posting collection with parent',parentId)
    clowderURL = 'http://'+zip_extractor_config.clowderhost+':'+zip_extractor_config.clowderport+'/'
    collection = pyclowderapi.get_collection(clowderURL,zip_extractor_config.playserverKey,
                                               folderName,'',parentId)
    return collection['id']

def postDataset(folder,parentId):
    folderName = folder[folder.rfind('/')+1:]
    clowderURL = 'http://'+zip_extractor_config.clowderhost + ':' + zip_extractor_config.clowderport+'/'
    dataset = pyclowderapi.create_dataset(clowderURL,
                                           folderName,'',None,None,zip_extractor_config.playserverKey,parentId )
    #print('posting dataset with parent', parentId)
    return dataset

def postFileToDataset(folder,currentFile,datasetId,theZip):
    currentPath = folder+'/'+currentFile
    clowderURL = 'http://' + zip_extractor_config.clowderhost + ':' + zip_extractor_config.clowderport + '/'

    pyclowderapi.upload_files_to_dataset(clowderURL,datasetId,[currentPath],metadata={},username=None,password=None,
                                         secret_key=zip_extractor_config.playserverKey)

def processFolder(folder, parentId,parameters,theZip):
    rootId = ""
    contents = os.listdir(folder)
    folderName = folder[folder.rfind('/')+1:]
    files_inside = []
    folders_inside = []
    for item in contents:
        try:
            subcontents = os.listdir(folder+'/'+item)
            folders_inside.append(folder+'/'+item)
        except:
            if (item != '.DS_Store'):
                files_inside.append(item)

    #collection case
    if ((len(files_inside) == 0) and (len(folders_inside) > 0)):
        collectionId = postCollection(folder,parentId)
        rootId = collectionId
        for each in folders_inside:
            processFolder(each,collectionId,parameters,theZip)
    #dataset case
    elif ((len(files_inside) > 0) and (len(folders_inside) == 0)):
        #if the root level is a dataset, it sould be a collection
        if (parentId == ""):
            parentId = postCollection(folder,"")
        datasetId = postDataset(folder,parentId)
        for each in files_inside:
            #print(folder+'/'+each,'a file inside')
            postFileToDataset(folder,each,datasetId,theZip)
    elif ((len(files_inside) > 0) and (len(folders_inside) > 0)):
        collectionId = postCollection(folder, parentId)
        for each in folders_inside:
            processFolder(each,collectionId,parameters,theZip)

        datasetId = postDataset(folder,parentId)
        for each in files_inside:
            postFileToDataset(folder,each,datasetId,theZip)
    return rootId

if __name__ == "__main__":
    main()
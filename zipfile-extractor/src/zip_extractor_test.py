import pyclowder
import pyclowder.api as pyclowderapi
import sys
import pika
import json
import logging
import zipfile
import tempfile
import pyclowder.extractors as extractors
import zip_extractor_config
import requests
import os

global logger
logging.basicConfig(format='%(levelname)-7s : %(name)s -  %(message)s', level=logging.WARN)
logging.getLogger('pyclowder.extractors').setLevel(logging.INFO)

def create_collection_from_zip(parameters):
    logger.info('processing zip file')
    #create temp file

    #write zipfile to temp file

    #iterate over all contents of zipfile

def postCollection(folder, parentId):
    folderName = folder[folder.rfind('/') + 1:]
    print('posting collection with parent',parentId)
    clowderURL = "http://"+zip_extractor_config.clowderhost+':'+zip_extractor_config.clowderport+"/"
    collectionId = pyclowderapi.get_collection(clowderURL,zip_extractor_config.playserverKey,
                                               folderName,'',parentId)

def postDataset(folder,parentId):
    folderName = folder[folder.rfind('/')+1:]
    clowderURL = "http://"+zip_extractor_config.clowderhost + ':' + zip_extractor_config.clowderport+"/"
    datasetId = pyclowderapi.create_dataset(clowderURL,
                                           folderName,'',None,None,zip_extractor_config.playserverKey,parentId )
    print('posting dataset with parent', parentId)

def postFileToDataset(file,datasetId):
    print('posting file',file,'to dataset',datasetId)

def processFolder(folder, parentId,parameters):
    contents = os.listdir(folder)
    folderName = folder[folder.rfind('/')+1:]
    files_inside = []
    folders_inside = []
    for item in contents:
        try:
            subcontents = os.listdir(folder+'/'+item)
            folders_inside.append(folder+'/'+item)
        except:
            if (item != '.DS_Store'):
                files_inside.append(item)

    #collection case
    if ((len(files_inside) == 0) and (len(folders_inside) > 0)):
        collectionId = postCollection(folder,parentId)
        for each in folders_inside:
            processFolder(each,parentId,parameters)
    #dataset case
    elif ((len(files_inside) > 0) and (len(folders_inside) == 0)):
        datasetId = postDataset(folder,parentId)
        for each in files_inside:
            postFileToDataset(each,datasetId)
    elif ((len(files_inside) > 0) and (len(folders_inside) > 0)):
        collectionId = postCollection(folder, parentId)
        for each in folders_inside:
            processFolder(each,collectionId,parameters)

        datasetId = postDataset(folder,parentId)
        for each in files_inside:
            postFileToDataset(file,datasetId)



def processLevel(currentName,currentLevel, previousLevelId):
    logger.info('processing',currentLevel)

zipFile = '/Users/todd_n/Desktop/Pole figures.zip'

parentId = "58570af7d4c6af93b8cd9bbf"
#postCollection('/testCollection',parentId)
postDataset('/theLateGreatPlate',parentId)
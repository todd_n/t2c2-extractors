import matplotlib
matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt

def getMetadata(linesFromFile):
    lastLine = linesFromFile.index('$DATE_MEA:\t\n')
    metadata_raw = linesFromFile[0:lastLine+1]
    metadata_fixed = []
    for each in metadata_raw:
        currrent = each
        current = current.rstrip('\t\n')
        metadata_fixed.append(current)
    return metadata_fixed





def fixNumericalData(linesOfData):
    fixed_x = []
    fixed_y = []
    for each in linesOfData:
        current = each
        current = current.rstrip('\n')
        current = current.split('\t')
        x_value = int(current[0])
        y_value = int(current[1])

        fixed_x.append(x_value)
        fixed_y.append(y_value)

        return [fixed_x,fixed_y]

def makePlot(lines,targetFile):
    data = fixNumericalData(lines)
    x_data = np.array(data[0])
    y_data = np.array(data[1])

    plt.plot(x_data, y_data, color='blue', linestyle='solid')
    plt.savefig(targetFile)
    plt.clf()
import numpy as np
import matplotlib.pyplot as plt

test_file = '/Users/helium/Desktop/FW__Use_of_SIMNRA_with_MAESTRO_for_Windows/Au_Cu_Si_01.spe'
test_plot = '/Users/helium/Desktop/plot.png'

def getLines(pathToFile):
    f = open(pathToFile, 'r')
    raw_lines = f.readlines()
    lines = []
    f.close()
    for each in raw_lines:
        fixed = each.rstrip('\n')
        fixed = each.rstrip('\r')
        fixed = each.strip()
        lines.append(fixed)
    return lines

def getDataFromLines(lines):
    start = 0
    end = 0
    for i in range(0, len(lines)):
        if '$DATA' in lines[i]:
            start = (i+2)
        if '$ROI' in lines[i]:
            end = i
    raw_data = lines[start:end]
    data = []
    for i in range(0, len(raw_data)):
        current = int(raw_data[i])
        data.append(current)
    return data

def findNextKey(current_index,lines):
    next_key_index = current_index
    for i in range(current_index+1,len(lines)):
        if lines[i][0] == '$':
            next_key_index = i
            return next_key_index
    return next_key_index

def getValueForKey(key_index,lines):
    next_key = findNextKey(key_index,lines)
    value = []
    if (next_key > key_index +2):
        for i in range(key_index+1,next_key):
            value.append(lines[i])
        return value
    else:
        value = lines[key_index+1]
        return value

def getMetadataFromLines(lines):
    metadata = dict()
    data_start = 0
    data_end  = 0
    for i in range(0, len(lines)):
        if '$DATA' in lines[i]:
            data_start = i
        if '$ROI' in lines[i]:
            data_end = i
    counter = 0
    while (counter < len(lines)):
        if (counter >= data_start and counter < data_end):
            counter = data_end
        else:
            current_line = lines[counter]
            if (current_line[0]=='$'):
                value = getValueForKey(counter,lines)
                metadata[current_line]=value
                next_key_index = findNextKey(counter,lines)
                if (next_key_index == counter):
                    counter = len(lines)+1
                else:
                    counter = next_key_index
    return metadata



def plotData(the_data,targetFile):
    x_data = np.arange(0, len(the_data))
    y_data = np.array(the_data)
    plt.plot(x_data, y_data, color='red', linestyle='solid')
    plt.xlabel("channel")
    plt.ylabel("counts")
    plt.savefig(targetFile)
    plt.clf()


linesFromFile = getLines(test_file)

data = getDataFromLines(linesFromFile)

mdata = getMetadataFromLines(linesFromFile)

plotData(data, test_plot)

print("done")
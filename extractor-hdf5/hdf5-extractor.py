#!/usr/bin/env python

import logging
import os
import subprocess
import tempfile
import re
import hdf5_helper
import json
import requests
from hdf5_extractor_config import *
import pyclowder.extractors as extractors

"""

to install pyclowder :

pip install git+https://opensource.ncsa.illinois.edu/stash/scm/cats/pyclowder.git

"""



def main():
    global extractorName, messageType, rabbitmqExchange, rabbitmqURL, logger

    #set logging
    logging.basicConfig(format='%(levelname)-7s : %(name)s -  %(message)s', level=logging.INFO)
    logging.getLogger('pymedici.extractors').setLevel(logging.DEBUG)
    logger = logging.getLogger(extractorName)
    logger.setLevel(logging.DEBUG)

    #connect to rabbitmq
    extractors.connect_message_bus(extractorName=extractorName,
                                   messageType=messageType,
                                   processFileFunction=process_file,
                                   rabbitmqExchange=rabbitmqExchange,
                                   rabbitmqURL=rabbitmqURL)

# ----------------------------------------------------------------------
# Process the file and upload the results
def process_file(parameters):

    global imageBinary, imageType, imageThumbnail, imagePreview
    global previewBinary, previewType, previewCommand


    if imageBinary:
        execute_command(parameters, imageBinary, imageThumbnail, imageType, True)
        execute_command(parameters, imageBinary, imagePreview, imageType, False)
    if previewBinary:
        execute_command(parameters, previewBinary, previewCommand, previewType, False)

#add here post metadata to file
def post_medatadata(parameters,fileid,host,fpath,*args):
    metadata = hdf5_helper.get_metadata_from_hdf5_returns_dictionary(parameters['inputfile'])
    pass

def execute_command(parameters, binary, commandline, ext, thumbnail=False):
    global logger

    (fd, tmpfile)=tempfile.mkstemp(suffix='.' + ext)
    try:
        # close tempfile
        os.close(fd)
        inputFile = parameters['inputfile']
        temp_image = None
        metadata = None
        try:
            unpacked_hdf5 = hdf5_helper.unpack_hdf5(inputFile)
            temp_image = hdf5_helper.get_image_tempfile_from_unpacked_hdf5(unpacked_hdf5)
            metadata = hdf5_helper.get_metadata_dict_from_unpacked_hdf5(unpacked_hdf5)
        except BaseException as e:
            logger.error("Could not unpack hdf5",str(e))

        #these methods will be replaced later
        #temp_tif = hdf5_helper.get_tiff_from_hdf5_returns_tempfile(inputFile)
        #metadata = hdf5_helper.get_metadata_from_hdf5_returns_dictionary(inputFile)

        try:
            metadata_json = json.dumps(metadata)
            fileid = parameters['id']
            host = parameters['host']
            playserverKey = parameters['secretKey']
            headers={'Content-Type': 'application/json'}
            url=host + 'api/files/' + fileid + '/metadata' + '?key=' + playserverKey
            r = requests.post(url, headers=headers, data=metadata_json)
            r.raise_for_status()
            extractors.upload_file_metadata(metadata_json,parameters)
        except BaseException as be:
            logger.error('could not upload metadata', str(be))

        commandline = commandline.replace('@BINARY@', binary)
        commandline = commandline.replace('@INPUT@', temp_image)
        commandline = commandline.replace('@OUTPUT@', tmpfile)

        # split command line
        p = re.compile(r'''((?:[^ "']|"[^"]*"|'[^']*')+)''')
        commandline = p.split(commandline)[1::2]


        # execute command
        x = subprocess.check_output(commandline, stderr=subprocess.STDOUT)
        if x:
            logger.debug(binary + " : " + x)

        if(os.path.getsize(tmpfile) != 0):
            # upload result
            if thumbnail:
                extractors.upload_thumbnail(thumbnail=tmpfile, parameters=parameters)
            else:
                extractors.upload_preview(previewfile=tmpfile, parameters=parameters)
    except subprocess.CalledProcessError as e:
        logger.error(binary + " : " + str(e.output))
        raise
    finally:
      try:
        os.remove(tmpfile)
      except BaseException as e:
        logger.error(str(e))

if __name__ == "__main__":
    main()

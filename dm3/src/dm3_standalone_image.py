import sys
import dm3_metadata_extractor
import dm3reader_v072

inputFile = sys.argv[1]
imageOutput = sys.argv[2]



try:
    print("attempting to get image for file " + inputFile)
    #metadata = dm3reader_v072.parseDM3(inputFile)
    image = dm3reader_v072.getDM3FileInfo(inputFile,True,tn_file=imageOutput)
except:
    print("error reading or writing metadata for file " + inputFile)


import sys
import dm3_metadata_extractor
import dm3reader_v072

inputFile = sys.argv[1]
metadataOutput = sys.argv[2]

try:
    print("attempting to get metadata for file " + inputFile)
    metadata = dm3reader_v072.extract_dm3_metadata(inputFile)
    keys = metadata.keys()
    numKeys = len(keys)
    print("extracted "  + str(numKeys) + " metadata items from " + inputFile)
    print("attempting to write metadata to text file")
    f = open(metadataOutput,'wb')
    f.write(str(metadata))
    f.close()
except:
    print("error reading or writing metadata for file " + inputFile)


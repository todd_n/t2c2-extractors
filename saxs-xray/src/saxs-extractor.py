import saxs_xray_config
import saxs
import logging
import os
import requests
import json
import pyclowder.extractors as extractors
import tempfile

def main():
    global extractorName, messageType, rabbitmqExchange, rabbitmqURL, registrationEndpoints,logger

    receiver = 'saxs.py-xray-extractor'
    extractorName = saxs_xray_config.extractorName
    messageType =  saxs_xray_config.messageType
    rabbitmqExchange = saxs_xray_config.rabbitmqExchange
    rabbitmqURL = saxs_xray_config.rabbitmqURL
    playserverKey = saxs_xray_config.playserverKey


    logging.basicConfig(format='%(levelname)-7s : %(name)s -  %(message)s', level=logging.WARN)
    logging.getLogger('pyclowder.extractors').setLevel(logging.INFO)

    logger = logging.getLogger(receiver)
    logger.setLevel(logging.DEBUG)

    extractors.connect_message_bus(extractorName=extractorName, messageType=messageType, processFileFunction=processFile,
        checkMessageFunction=check_message, rabbitmqExchange=rabbitmqExchange, rabbitmqURL=rabbitmqURL)

def check_message(parameters):
    return True

def processFile(parameters):
    input_file = parameters['inputfile']

    (fd, tn_file) = tempfile.mkstemp(suffix=".png")
    isXray = saxs.checkFile(input_file)
    if (isXray):
        try:
            saxs.makeFile(input_file, tn_file)
            preview_id = extractors.upload_preview(tn_file,parameters,None)
            upload_preview_title(preview_id,'Plot of All Points',parameters)
        except:
            logger.error("could not process file " + input_file)

        try:
            os.remove(tn_file)
        except:
            logger.error("Could not remove temporary file")

        (fd, tn_file) = tempfile.mkstemp(suffix=".png")
        try:
            saxs.makePeakViewFile(input_file, tn_file)
            preview_id = extractors.upload_preview(tn_file,parameters,None)
            upload_preview_title(preview_id,'Plot of Peak',parameters)
        except:
            logger.error("could not process file " + input_file)
    try:
        os.remove(tn_file)
    except:
        logger.error("Could not remove temporary file")

def upload_preview_title(previewid, title_name,parameters):
    key = parameters['secretKey']
    title = dict()
    title['title'] = title_name
    host = parameters['host']
    if (not host.endswith("/")):
        host = host + "/"
    url = host + 'api/previews/'+previewid+'/title?key='+key

    headers = {'Content-Type': 'application/json'}

    if url:
        r = requests.post(url, headers=headers, data=json.dumps(title), verify=False);
        r.raise_for_status()
    return r

if __name__ == "__main__":
    main()
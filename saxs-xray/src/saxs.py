import matplotlib
matplotlib.use('Agg')
import numpy as np

import matplotlib
# Force matplotlib to not use X window display
matplotlib.use('Agg')

import matplotlib.pyplot as plt
from matplotlib import colors
import six
import re

xray_file = '/Users/todd_n/Desktop/SAXS-selected/Schwartz-SAXS.dat'

output_file = '/Users/todd_n/Desktop/xray-CSV.csv'

def checkFile(pathToFile):
    f = open(pathToFile,'r')
    lines = f.readlines()
    f.close()
    first_line = lines[0]
    isXray = False
    first_line = re.sub('\s+', '', first_line)
    if (first_line == 'QNPVSolCapNVP-sol'):
        isXray = True
    return isXray

def processFile(pathToFile):
    clrs = list(six.iteritems(colors.cnames))
    f = open(pathToFile,'r')
    lines = f.readlines()
    f.close()
    first_line = lines[0]
    isXray = False
    first_line = re.sub('\s+','',first_line)
    if (first_line == 'QNPVSolCapNVP-sol'):
        isXray = True
        first_line = ['Q','NPV','Sol','Cap','NPV-sol']
    second_line = lines[1]
    second_line = re.sub('\s+', '', second_line)
    content = lines[2:]
    fixed_content = []
    for line in content:
        fixed_line = processLine(line)
        fixed_content.append(fixed_line)
    return fixed_content


def processLine(line):
    new_line = []
    line = line.split('\t')
    for each in line:
        each = each.rstrip('\r\n')
        new_line.append(float(each))
    return new_line

def createNumpyArrays(pathToFile):
    content = processFile(pathToFile)
    first_line = content[0]
    num_entries = len(first_line)
    arrays = []
    for i in range(0,num_entries):
        current = []
        arrays.append(current)

    for line in content:
        for i in range(0,len(line)):
            current_value = line[i]
            arrays[i].append(current_value)

    numpy_arrays = []
    for each in arrays:
        numpy_array = np.array(each)
        numpy_arrays.append(numpy_array)

    x_points = []
    y1_points = []
    y2_points = []
    y3_points = []
    y4_points = []

    for line in content:
        x_points.append(line[0])
        y1_points.append(line[1])
        y2_points.append(line[2])
        y3_points.append(line[3])
        y4_points.append(line[4])
    x_numpy_array = np.array(x_points)#[160:460])
    y1_numpy_array = np.array(y1_points)#[160:460])
    y2_numpy_array = np.array(y2_points)#{160:460])
    y3_numpy_array = np.array(y3_points)#[160:460])
    y4_numpy_array = np.array(y4_points)#[160:460])
    return [x_numpy_array,y1_numpy_array,y2_numpy_array,y3_numpy_array,y4_numpy_array,]

def createNumpyArraysWithLimits(arrays,start,end):
    x_numpy_array =  arrays[0][start:end]
    y1_numpy_array = arrays[1][start:end]
    y2_numpy_array = arrays[2][start:end]
    y3_numpy_array = arrays[3][start:end]
    y4_numpy_array = arrays[4][start:end]
    return [x_numpy_array,y1_numpy_array,y2_numpy_array,y3_numpy_array,y4_numpy_array,]


def makeFile(pathToFile,target):
    headings = ['Q','NPV','Sol','Cap','NPV-sol']
    arrays = createNumpyArrays(pathToFile)
    color_array = getColorArray()
    filled_markers = getMarkers()
    plt.plot(arrays[0],arrays[1],color = 'red', linestyle='solid')
    plt.plot(arrays[0],arrays[2],color = 'blue', linestyle='solid')
    plt.plot(arrays[0], arrays[3], color='green',  linestyle='solid')
    plt.plot(arrays[0], arrays[4], color='orange',  linestyle='solid')
    plt.legend(['NPV','SOL','Cap','NPV-sol'],loc = 'upper right')
    #plt.plot(arrays[0], arrays[1],color_array[0], arrays[0], arrays[2], color_array[10], arrays[0], arrays[3], color_array[20], arrays[0],arrays[4], color_array[30])
    plt.xlabel("Q(nm^-1)")
    plt.ylabel("Intensity (a.u.)")
    plt.savefig(target)

def makePeakViewFile(pathToFile,pathToImage):
    arrays = createNumpyArrays(pathToFile)
    largestPeak = findLargetPeak(arrays[1])
    startPeakIndex = largestPeak[0]
    endPeakIndex = largestPeak[1]
    peakSize = endPeakIndex - startPeakIndex
    start = max(0, startPeakIndex - peakSize)
    end = min(len(arrays[1]),endPeakIndex + peakSize)
    smaller_arrays = createNumpyArraysWithLimits(arrays,start,end)
    color_array = getColorArray()
    filled_markers = getMarkers()
    plt.close()
    plt.plot(smaller_arrays[0], smaller_arrays[1], color='red', linestyle='solid')
    plt.plot(smaller_arrays[0], smaller_arrays[2], color='blue',  linestyle='solid')
    plt.plot(smaller_arrays[0], smaller_arrays[3], color='green',  linestyle='solid')
    plt.plot(smaller_arrays[0], smaller_arrays[4], color='orange',  linestyle='solid')
    plt.legend(['NPV', 'SOL', 'Cap', 'NPV-sol'], loc='upper right')
    # plt.plot(arrays[0], arrays[1],color_array[0], arrays[0], arrays[2], color_array[10], arrays[0], arrays[3], color_array[20], arrays[0],arrays[4], color_array[30])
    plt.xlabel("Q(nm^-1)")
    plt.ylabel("Intensity (a.u.)")
    plt.savefig(pathToImage)


def findIncreases(array):
    for i in range(1,len(array)):
        previous = array[i-1]
        current = array[i]
        if (previous >= current):
            None
        else:
            None

def compareWithCurrentMin(array,other_array):
    current_min = 99999999
    for i in range(0,len(array)):
        other_value = other_array[i]
        current = array[i]
        if (i == 0):
            current_min = array[i]
        else:
            if (current < current_min):
                current_min = array[i]
        if (current > current_min):
            None



def getColorArray():
    c = list(six.iteritems(colors.cnames))
    color_names = []
    for each in c:
        color_names.append(str(each[0]))
    return color_names

def getMarkers():
    filled_markers = ('o', 'v', '^', '<', '>', '8', 's', 'p', '*', 'h', 'H', 'D', 'd')
    return filled_markers

def findPeaks(array):

    currentMin = array[0]

    currently_in_peak = False
    peak_start = 0
    peak_end = 0
    current_peak_count = 0
    largest_peak = 0
    peaks = []
    #a peak has start index, end index, and size

    for i in range(0,len(array)):
        current = array[i]
        if (current < currentMin):
            currentMin = current
        if (current > currentMin ):
            if ( currently_in_peak == False):
                currently_in_peak = True
                current_peak_count += 1
                peak_start = i
            elif (currently_in_peak == True):
                current_peak_count += 1
        if ((current <= currentMin) and (currently_in_peak == True)):
            currently_in_peak = False
            peak_stop = i
            if (current_peak_count > largest_peak):
                peaks.append([peak_start,peak_stop,current_peak_count])
                largest_peak = current_peak_count
                current_peak_count = 0

    return peaks

def findLargetPeak(array):
    peaks = findPeaks(array)
    if (len(peaks) > 0):
        largest = peaks[-1]
        return largest
    else:
        return None

test_array = [100,95,90,85,80,81,82,83,84,85,84,83,82,81,80,75,70,65,60
              ,61,62,63,64,65,66,65,64,63,62,61,60]



#names = getColorArray()
#arrays = createNumpyArrays(xray_file)
#p = findPeaks(arrays[1])
#lrgsest = findLargetPeak(arrays[1])
#compareWithCurrentMin(arrays[1],arrays[0])
#findIncreases(arrays[1])

#makeFile(xray_file,'test6.png')
#makePeakViewFile(xray_file,'test7.png')

import matplotlib
matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
import json, xmljson
from lxml.etree import fromstring, tostring
import xmltodict
from collections import OrderedDict

sampleFile = '/Users/helium/Desktop/XRR-Ag-on-Glass-NewEbeam.xrdml'
sampleFile2 = '/Users/helium/Desktop/224RLM-3-2120.xrdml'
sampleFile3 = '/Users/helium/Desktop/2thOm-XRD-Au-Cu_on_Si-Tao-RBScalibration.xrdml'

def jsonFromXMLFile(pathToFile):
    f = open(pathToFile,'rb')
    content = f.read()
    f.close()
    xml = fromstring(content)
    as_json = json.dumps(xmljson.badgerfish.data(xml))
    return as_json

def newGetMinStepSize(content_as_dict):
    content_as_dict = dict(content_as_dict)
    xrdMeasurements = content_as_dict['xrdMeasurements']
    comment = xrdMeasurements['comment']
    entry = comment['entry']
    _2theta_omega_info = str(entry[1])
    _2theta_omega_info = _2theta_omega_info.split(';')
    _2theta_pair = (_2theta_omega_info[1]).split(':')
    _2theta_step = float(_2theta_pair[1])

    _omega_pair = (_2theta_omega_info[2]).split(':')
    _omega_step = float(_omega_pair[1])
    return [_2theta_step, _omega_step]

def getIntensities(content_as_dict):
    result = []
    content_as_dict = dict(content_as_dict)
    xrdMeasurements = content_as_dict['xrdMeasurements']
    xrdMeasurement = xrdMeasurements['xrdMeasurement']
    scan = xrdMeasurement['scan']
    if type(scan) == OrderedDict:
        dataPoints = scan['dataPoints']
        intensities = dataPoints['intensities']
        # intensities = positions['intensities']

        text = intensities['#text']
        intesities_values = text.split(' ')
        for i in range(0, len(intesities_values)):
            current = intesities_values[i]
            new_value = int(current)
            result.append(new_value)
        return result
    elif type(scan) == list:
        scan0 = scan[0]
        dataPoints = scan0['dataPoints']
        intensities = dataPoints['intensities']
        text = intensities['#text']
        intesities_values = text.split(' ')
        for i in range(0, len(intesities_values)):
            current = intesities_values[i]
            new_value = int(current)
            result.append(new_value)
        return result
    else:
        return None

def getMinMaxOmega(content_as_dict):
    content_as_dict = dict(content_as_dict)
    xrdMeasurements = content_as_dict['xrdMeasurements']
    xrdMeasurement = xrdMeasurements['xrdMeasurement']
    scan = xrdMeasurement['scan']
    if (type(scan) == OrderedDict):
        dataPoints = scan['dataPoints']
        positions = dataPoints['positions']
        for each in positions:
            if each['@axis'] == 'Omega':
                _omega = each
                if ('startPosition' in _omega) and ('endPosition' in _omega):
                    omega_startPosition = float(str(_omega['startPosition']))
                    omega_endPosition = float(str(_omega['endPosition']))
                    return [omega_startPosition, omega_endPosition]
                else:
                    return None
    elif (type(scan) == list):
        scan0 = scan[0]
        dataPoints = scan0['dataPoints']
        positions = dataPoints['positions']
        for each in positions:
            if each['@axis'] == 'Omega':
                _omega = each
                if ('startPosition' in _omega) and ('endPosition' in _omega):
                    omega_startPosition = float(str(_omega['startPosition']))
                    omega_endPosition = float(str(_omega['endPosition']))
                    return [omega_startPosition, omega_endPosition]
                else:
                    return None

    else:
        return None

def getMinMax2theta(content_as_dict):
    content_as_dict = dict(content_as_dict)
    xrdMeasurements = content_as_dict['xrdMeasurements']
    xrdMeasurement = xrdMeasurements['xrdMeasurement']
    scan = xrdMeasurement['scan']
    if type(scan) == OrderedDict:
        dataPoints = scan['dataPoints']
        positions = dataPoints['positions']
        for each in positions:
            if each['@axis'] == '2Theta':
                print("has2theta")
                _2Theta = each
                _2Theta_startPosition = float(str(_2Theta['startPosition']))
                _2Theta_endPosition = float(str(_2Theta['endPosition']))
                return [_2Theta_startPosition, _2Theta_endPosition]
    elif type(scan) == list:
        scan0 = scan[0]
        dataPoints = scan0['dataPoints']
        positions = dataPoints['positions']
        for each in positions:
            if each['@axis'] == '2Theta':
                print("has2theta")
                _2Theta = each
                _2Theta_startPosition = float(str(_2Theta['startPosition']))
                _2Theta_endPosition = float(str(_2Theta['endPosition']))
                return [_2Theta_startPosition, _2Theta_endPosition]

def plot_x_2theta_y_counts(intensities,target):
    x_values = np.arange(len(intensities))
    y_values = np.array(intensities)
    plt.plot(x_values, y_values, color='blue', linestyle='solid')
    plt.xlabel("2Theta")
    plt.ylabel("counts")
    plt.savefig(target)
    plt.clf()

fd = open(sampleFile, 'rb')
fd2 = open(sampleFile2, 'rb')
fd3 = open(sampleFile3, 'rb')
doc = xmltodict.parse(fd.read())
doc2 = xmltodict.parse(fd2.read())
doc3 = xmltodict.parse(fd3.read())

fd.close()
fd2.close()
fd3.close()

try:
    step_size = newGetMinStepSize(doc)
    minMax2theta = getMinMax2theta(doc)
    min2theta = minMax2theta[0]
    max2theta = minMax2theta[1]
    a3 = getMinMaxOmega(doc)
    counts = getIntensities(doc3)
    f = open('/Users/helium/Desktop/test2.png','w')
    plot_x_2theta_y_counts(counts,f)
    f.close()
    max = min2theta + (step_size[0]*(len(counts)))
    print(max,max2theta)
except:
    print("fail")

try:
     b = newGetMinStepSize(doc2)
except:
    print("fail")
try:
     c = newGetMinStepSize(doc3)
except:
    print("fail")
print('done')
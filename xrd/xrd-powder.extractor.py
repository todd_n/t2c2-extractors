import extractor_config
import xrd_xml_convert
import xrd_xpert
import xmltodict
import logging
import os
import requests
import json
import pyclowder.extractors as extractors
import tempfile

def main():
    global extractorName, messageType, rabbitmqExchange, rabbitmqURL, registrationEndpoints,logger

    receiver = 'xrd-xmlrd-extractor'
    extractorName = extractor_config.extractorName
    messageType =  extractor_config.messageType
    rabbitmqExchange =extractor_config.rabbitmqExchange
    rabbitmqURL = extractor_config.rabbitmqURL
    playserverKey =extractor_config.playserverKey


    logging.basicConfig(format='%(levelname)-7s : %(name)s -  %(message)s', level=logging.WARN)
    logging.getLogger('pyclowder.extractors').setLevel(logging.INFO)

    logger = logging.getLogger(receiver)
    logger.setLevel(logging.DEBUG)

    extractors.connect_message_bus(extractorName=extractorName, messageType=messageType, processFileFunction=processFile,
        checkMessageFunction=check_message, rabbitmqExchange=rabbitmqExchange, rabbitmqURL=rabbitmqURL)

def check_message(parameters):
    return True

def processFile(parameters):
    input_file = parameters['inputfile']


    if (input_file):
        (fd, tn_file_2theta) = tempfile.mkstemp(suffix=".png")
        (fd1,tn_file_omega) = tempfile.mkstemp(suffix='.png')
            # create image preview
        try:
            metadata = xrd_xml_convert.jsonFromXMLFile(input_file)
            open_file = open(input_file,'rb')
            content = open_file.read()
            open_file.close()
            content_as_dictionary = xmltodict.parse(content.read())
            data = xrd_xml_convert.getDataForPlots()
            #counts = data[0]
            #_theta = data[1]
            #_omega = data[2]
            #xrd_xml_convert.plot_x_omega_y_counts(_omega,counts,tn_file_omega)
            #xrd_xml_convert.plot_x_2theta_y_counts(_theta,counts,tn_file_2theta)
            data = xrd_xpert.getIntensities(content_as_dictionary)
            xrd_xpert.plot_x_2theta_y_counts(data,tn_file_2theta)
            preview_id = extractors.upload_preview(tn_file_2theta, parameters, None)
            upload_preview_title(preview_id, 'counts', parameters)
            #preview_id_2theta = extractors.upload_preview(tn_file_2theta,parameters,None)
            #upload_preview_title(preview_id_omega, 'counts/2theta', parameters)
            try:
                addMetadataToXMLXRDFile(parameters, parameters['host'], parameters['fileid'], json.dumps(metadata))
            except:
                logger.error("could not post metadata")
        except:
            logger.error("could not process file " + input_file)

        try:
            os.remove(tn_file_2theta)
        except:
            logger.error("Could not remove temporary file")

        try:
            os.remove(tn_file_omega)
        except:
            logger.error("Could not remove temporary file")

def upload_preview_title(previewid, title_name,parameters):
    key = parameters['secretKey']
    title = dict()
    title['title'] = title_name
    host = parameters['host']
    if (not host.endswith("/")):
        host = host + "/"
    url = host + 'api/previews/'+previewid+'/title?key='+key

    headers = {'Content-Type': 'application/json'}

    if url:
        r = requests.post(url, headers=headers, data=json.dumps(title), verify=False);
        r.raise_for_status()
    return r

def addMetadataToXMLXRDFile(parameters,host,file_id,metaDataJson):
    headers = {'Content-Type': 'application/json'}
    url = host +'/api/files/' + file_id + '/metadata?key=' + parameters['secretKey']

    r = requests.post(url, headers=headers, data=metaDataJson, verify=False)
    r.raise_for_status()

if __name__ == "__main__":
    main()

# -*- coding: utf-8 -*-
import matplotlib
matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
import json, xmljson
from lxml.etree import fromstring, tostring
import xmltodict
from collections import OrderedDict

sampleFile = '/Users/helium/Desktop/2thOm-XRD-Au-Cu_on_Si-Tao-RBScalibration.xrdml'
sampleFile2 = '/Users/helium/Desktop/224RLM-3-2120.xrdml'

def jsonFromXMLFile(pathToFile):
    f = open(pathToFile,'rb')
    content = f.read()
    f.close()
    xml = fromstring(content)
    as_json = json.dumps(xmljson.badgerfish.data(xml))
    return as_json

def getMinimumStepSizes(content_as_dict):
    content_as_dict = dict(content_as_dict)
    xrdMeasurements = content_as_dict['xrdMeasurements']
    comment = xrdMeasurements['comment']
    entry_ = comment['entry']
    entry = content_as_dict[content_as_dict.keys()[0]][(content_as_dict[content_as_dict.keys()[0]]).keys()[4]]
    entry_list = entry[entry.keys()[0]]
    _2theta_omega_info = str(entry_list[1])
    _2theta_omega_info = _2theta_omega_info.split(';')
    _2theta_pair = (_2theta_omega_info[1]).split(':')
    _2theta_step = float(_2theta_pair[1])

    _omega_pair = (_2theta_omega_info[2]).split(':')
    _omega_step = float(_omega_pair[1])
    
    return [_2theta_step,_omega_step]

def newGetMinStepSize(content_as_dict):
    content_as_dict = dict(content_as_dict)
    xrdMeasurements = content_as_dict['xrdMeasurements']
    comment = xrdMeasurements['comment']
    entry = comment['entry']
    _2theta_omega_info = str(entry[1])
    _2theta_omega_info = _2theta_omega_info.split(';')
    _2theta_pair = (_2theta_omega_info[1]).split(':')
    _2theta_step = float(_2theta_pair[1])

    _omega_pair = (_2theta_omega_info[2]).split(':')
    _omega_step = float(_omega_pair[1])
    return [_2theta_step, _omega_step]


def getMin2ThetaMinOmega(content_as_dict):
    content_as_dict = dict(content_as_dict)
    xmlrd_content = content_as_dict[content_as_dict.keys()[0]]
    x = content_as_dict['xrdMeasurements']
    print("value of key for xmlrd content",str(content_as_dict.keys()[0]))
    data = xmlrd_content[xmlrd_content.keys()[-1]]
    d = x['xrdMeasurement']
    print("value of key for data",str(xmlrd_content.keys()[-1]))
    measurements = data[data.keys()[8]]
    m = d['scan']
    print('type of scan',type(m))
    print("value of key for measurements",str(data.keys()[8]))
    #EDIT positions = measurements[measurements.keys(data.keys()[8])[5]]
    positions = measurements[measurements.keys()[5]]
    p = m['dataPoints']
    print("value of key for positions",str(measurements.keys()[5]))
    _2Theta_omega_info = positions[positions.keys()[0]]
    p['positions']
    print("value of key for _2Theta_omega_info",positions.keys()[0])
    _2Theta = _2Theta_omega_info[0]
    _2Theta_startPosition = float(str(_2Theta['startPosition']))
    _2Theta_endPosition = float(str(_2Theta['endPosition']))
    omega = _2Theta_omega_info[1]
    omega_startPosition = float(str(omega['startPosition']))
    omega_endPosition = float(str(omega['endPosition']))

    return [_2Theta_startPosition,omega_startPosition,_2Theta_endPosition,omega_endPosition]

def getMin__(content_as_dict):
    content_as_dict = dict(content_as_dict)
    xrdMeasurements = content_as_dict['xrdMeasurements']
    xrdMeasurement = xrdMeasurements['xrdMeasurement']
    scan = xrdMeasurement['scan']
    if (type(scan) == OrderedDict):
        dataPoints = scan['dataPoints']
        positions = dataPoints['positions']
        _2Theta = positions[0]
        _2Theta_startPosition = float(str(_2Theta['startPosition']))
        _2Theta_endPosition = float(str(_2Theta['endPosition']))
        _omega = positions[1]
        omega_startPosition = float(str(_omega['startPosition']))
        omega_endPosition = float(str(_omega['endPosition']))
        return [_2Theta_startPosition, omega_startPosition, _2Theta_endPosition, omega_endPosition]
    elif (type(scan) == list):
        scan0 = scan[0]
        dataPoints = scan0['dataPoints']
        positions = dataPoints['positions']
        _2Theta = positions[0]
        _2Theta_startPosition = float(str(_2Theta['startPosition']))
        _2Theta_endPosition = float(str(_2Theta['endPosition']))
        _omega = positions[1]
        omega_startPosition = float(str(_omega['startPosition']))
        omega_endPosition = float(str(_omega['endPosition']))
        return [_2Theta_startPosition, omega_startPosition, _2Theta_endPosition, omega_endPosition]



def getCounts(content_as_dict):
    content_as_dict = dict(content_as_dict)
    xmlrd_content = content_as_dict[content_as_dict.keys()[0]]
    data = xmlrd_content[xmlrd_content.keys()[-1]]
    measurements = data[data.keys()[8]]
    positions = measurements[measurements.keys()[5]]
    a= positions.keys()[-1]
    intensities = positions['intensities']
    counts = str(positions[positions.keys()[-1]]['#text']).split(' ')
    text = intensities['#text']
    for i in range(0, len(counts)):
        current = counts[i]
        new_value = int(current)
        counts[i] = new_value
    return counts

def getCounts2(content_as_dict):
    result = []
    content_as_dict = dict(content_as_dict)
    xrdMeasurements = content_as_dict['xrdMeasurements']
    xrdMeasurement = xrdMeasurements['xrdMeasurement']
    scan = xrdMeasurement['scan']
    dataPoints = scan['dataPoints']
    intensities = dataPoints['intensities']
    #intensities = positions['intensities']

    text = intensities['#text']
    intesities_values = text.split(' ')
    for i in range(0,len(intesities_values)):
        current = intesities_values[i]
        new_value = int(current)
        result.append(new_value)
    return result

asJson = jsonFromXMLFile(sampleFile)
asJson2 = jsonFromXMLFile(sampleFile2)

print("got them")

def getDataForPlots(content_as_dict):
    s_end = getMin__(content_as_dict)
    #min_step = newGetMinStepSize(content_as_dict)
    start_end_positions = getMin2ThetaMinOmega(content_as_dict)
    min_stepsizes = getMinimumStepSizes(content_as_dict)
    stepsize_2theta = min_stepsizes[0]
    stepsize_omega = min_stepsizes[1]

    start_2theta = start_end_positions[0]
    start_omega = start_end_positions[1]
    end_2theta = start_end_positions[2]
    end_omega = start_end_positions[3]
    counts = getCounts(content_as_dict)
    c2 = getCounts2(content_as_dict)
    _2theta_array = []
    _omega_array = []
    stepsize_2theta = ((end_2theta-start_2theta)/359)
    stepsize_omega = ((end_omega-start_omega)/359)
    for i in range(0,len(counts)):
        _2theta_array.append(start_2theta+(i*stepsize_2theta))
        _omega_array.append(start_omega+(i*stepsize_omega))
    return [counts,_2theta_array,_omega_array]

def plot_x_2theta_y_counts(_2theta_array,counts,target):
    x_values = np.array(_2theta_array)
    y_values = np.array(counts)
    plt.plot(x_values, y_values, color='blue', linestyle='solid')
    plt.xlabel("2Theta")
    plt.ylabel("counts")
    plt.savefig(target)
    plt.clf()

def plot_x_omega_y_counts(_omega_array,counts,target):
    x_values = np.array(_omega_array)
    y_values = np.array(counts)
    plt.plot(x_values, y_values, color='red', linestyle='solid')
    plt.xlabel("omega")
    plt.ylabel("counts")
    plt.savefig(target)
    plt.clf()

def testPlot(_2theta_array,_omega_array,counts,target):
    new_array = []
    for i in range(0,len(_omega_array)):
        value = (_2theta_array[i]/_omega_array[i])
        new_array.append(value)
    x_values = np.array(new_array)
    y_values = np.array(counts)
    plt.plot(x_values, y_values, color='red', linestyle='solid')
    plt.xlabel("2theata/omega")
    plt.ylabel("counts")
    plt.savefig(target)
    plt.clf()


fd = open(sampleFile,'rb')
fd2 = open(sampleFile2,'rb')
doc = xmltodict.parse(fd.read())
doc2 = xmltodict.parse(fd2.read())

fd.close()
fd2.close()

f = open('/Users/helium/Desktop/saxs.py-xray.txt', 'wb')
f.write(asJson)
f.close()
print('done')


v = getMin__(doc)
v1 = getMin__((doc2))

values = getDataForPlots(doc)
values = getDataForPlots(doc2)

counts = values[0]
_theta = values[1]
_omega = values[2]

plot_x_2theta_y_counts(_theta,counts,'/Users/helium/Desktop/theta.png')
plot_x_omega_y_counts(_omega,counts,'/Users/helium/Desktop/omega.png')


#testPlot(_theta,_omega,counts,'/Users/helium/Desktop/test1.png')
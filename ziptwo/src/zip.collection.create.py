#!/usr/bin/env python

import logging
import time
import pyclowder.extractors as extractors
import zip_helper
import zip_helper_2
import pika
from zip_config import *

"""

to install pyclowder :

pip install git+https://opensource.ncsa.illinois.edu/stash/scm/cats/pyclowder.git

"""



def main():
    global extractorName, messageType, rabbitmqExchange, rabbitmqURL, logger

    #set logging
    logging.basicConfig(format='%(levelname)-7s : %(name)s -  %(message)s', level=logging.INFO)
    logging.getLogger('pymedici.extractors').setLevel(logging.DEBUG)
    logger = logging.getLogger(extractorName)
    logger.setLevel(logging.DEBUG)

    n_retries = 0
    while(True):
        try:
            #connect to rabbitmq
            extractors.connect_message_bus(
                    extractorName=extractorName, 
                    messageType=messageType, 
                    processFileFunction=process_file,
                    rabbitmqExchange=rabbitmqExchange, 
                    rabbitmqURL=rabbitmqURL)
            break
        except pika.exceptions.AMQPConnectionError:
            n_retries += 1
            if n_retries == 5:
                break
            print 'Cannot connect to RabbitMQ server. Retry in 5s...'
            time.sleep(5)


def check_message(parameters):
    return True
# ----------------------------------------------------------------------
# Process the file and upload the results
def process_file(parameters):
    print(parameters['inputfile'])
    try:
        zip_helper_2.create_collection_from_zip(parameters)
    except Exception as e:
        print str(e)
    print("Finished with file !!")




if __name__ == "__main__":
    main()

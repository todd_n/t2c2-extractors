import json
import logging
import zipfile
import tempfile
import zip_config
import requests
import os
import errno
import shutil
import time

def delete_file_from_clowder(clowder_url, secret_key, file_id):
    to_send = '%sapi/files/%s/remove&key=%s' % (clowder_url, file_id,secret_key)
    r = requests.post('%sapi/files/%s/remove?key=%s' % (clowder_url, file_id,secret_key))
    if (r.status_code != 200):
        #error
        return None


    # fetch file and return
    # TODO does not work see MMDB-1685
    # r = requests.get('%sapi/files/%s?key=%s' %
    #                  (clowder_url, file_id, secret_key))
    # if (r.status_code != 200):
    #   log.error('Could not get the file : [%d] - %s)' %
    #             (r.status_code, r.text))
    #   return None
    # return r.json()
    return True

def unzipFileToTempDirectory(pathToZipFile):
    fd = tempfile.mkdtemp()

    zf = zipfile.ZipFile(pathToZipFile,'r')
    zf.extractall(fd)
    zf.close()
    try:
        del zf
        os.unlink(pathToZipFile)
    except Exception as e:
        print str(e)
    #TODO delete zipfile here
    return fd

def create_collection_from_zip(parameters):
    theZip = parameters['inputfile']
    print("unzipping file")
    temp = unzipFileToTempDirectory(theZip)
    type_of_temp = type(temp)
    print("done unzipping file")
    try:
        all_contents = os.listdir(temp)
        if (len(all_contents) == 1):
            theFolder = all_contents[0]
            print("Only one folder in zip",theFolder)
        else:
            foundContents = False
            for i in range (0,len(all_contents)):
                if (foundContents == False):
                    current = all_contents[i]
                    if (all_contents[i] != '__MACOSX'):
                        foundContents = True
                        theFolder = all_contents[i]
            print("More than one folder in zip, content in " , theFolder)
        currentRoot = ""
        currentRoot = processFolder(temp+'/'+theFolder,"",parameters)

        optional_parameters = ""
        try:
            optional_parameters = parameters['parameters']
        except Exception as e:
            print str(e)

        if (optional_parameters != ""):
            try:
                content = json.loads(optional_parameters)
                if ('loggedInUser' in content):
                    new_author_id = content['loggedInUser']
                if ('space' in content):
                    space = content['space']
                    if (space != ""):
                        add_collection_to_space(currentRoot,space,parameters)
            except:
                pass

        try:
            os.remove(temp)
        except Exception as e:
            print("error in deleting temp file")
            print str(e)
        clowder_url = 'http://' + zip_config.clowderhost + ':' + zip_config.clowderport + '/'
        delete_file_from_clowder(clowder_url,zip_config.playserverKey,parameters['id'])
        send_email(new_author_id,currentRoot)
    except Exception as e:
        print str(e)
    try:
        shutil.rmtree(temp)  # delete directory
    except OSError as exc:
        if exc.errno != errno.ENOENT:  # ENOENT - no such file or directory
            raise  # re-raise exception

    finally:
        if (os.path.isdir(temp)):
            try:
                os.remove(temp)
            except Exception as e:
                print str(e)

def changeCollectionAuthor(parameters,rootId, newAuthorId):
    clowder_url = 'http://' + zip_config.clowderhost + ':' + zip_config.clowderport + '/'
    secret_key = zip_config.playserverKey
    requestURL = clowder_url+'api/collections/'+rootId+'/changeOwner/'+newAuthorId+"?key="+secret_key
    headers = {'Content-type': 'application/json'}
    body = {'rootId': rootId, 'newUserId': newAuthorId, 'methodKey': zip_config.key_4ceed}
    r = requests.post(requestURL,data=json.dumps(body),headers=headers)

    #if (r.status_code != 200):
     #   logger.error('Problem changing owner of collection  : [%d] - %s)' %
      #        (r.status_code, r.text))

def change_collection_author(parameters,col_id):
    optional_parameters = ""
    try:
        optional_parameters = parameters['parameters']
    except Exception as e:
        print str(e)

    newOwner = ""
    if (optional_parameters != ""):
        try:
            content = json.loads(optional_parameters)
            if ('loggedInUser' in content):
                new_author_id = content['loggedInUser']
        except Exception as e:
            print str(e)
    clowder_url = 'http://' + zip_config.clowderhost + ':' + zip_config.clowderport + '/'
    secret_key = zip_config.playserverKey
    requestURL = clowder_url+'api/collections/'+col_id+'/changeOwnerCollection/'+new_author_id+"?key="+secret_key
    #headers = {'Content-type': 'application/json'}
    #body = {'rootId': col_id, 'newUserId': new_author_id, 'methodKey': zip_config.key_4ceed}
    r = requests.post(requestURL)
    return r

def change_dataset_author(parameters,ds_id):
    optional_parameters = ""
    try:
        optional_parameters = parameters['parameters']
    except Exception as e:
        print str(e)

    newOwner = ""
    if (optional_parameters != ""):
        try:
            content = json.loads(optional_parameters)
            if ('loggedInUser' in content):
                new_author_id = content['loggedInUser']
        except Exception as e:
            print str(e)
    clowder_url = 'http://' + zip_config.clowderhost + ':' + zip_config.clowderport + '/'
    secret_key = zip_config.playserverKey
    requestURL = clowder_url+'api/collections/'+ds_id+'/changeOwnerDataset/'+new_author_id+"?key="+secret_key
    #headers = {'Content-type': 'application/json'}
    #body = {'rootId': col_id, 'newUserId': new_author_id, 'methodKey': zip_config.key_4ceed}
    r = requests.post(requestURL)
    return r

def change_file_author(parameters, f_id):
    optional_parameters = ""
    try:
        optional_parameters = parameters['parameters']
    except Exception as e:
        print str(e)

    newOwner = ""
    if (optional_parameters != ""):
        try:
            content = json.loads(optional_parameters)
            if ('loggedInUser' in content):
                new_author_id = content['loggedInUser']
        except Exception as e:
            print str(e)
    clowder_url = 'http://' + zip_config.clowderhost + ':' + zip_config.clowderport + '/'
    secret_key = zip_config.playserverKey
    requestURL = clowder_url + 'api/collections/' + f_id + '/changeOwnerFile/' + new_author_id + "?key=" + secret_key
    # headers = {'Content-type': 'application/json'}
    # body = {'rootId': col_id, 'newUserId': new_author_id, 'methodKey': zip_config.key_4ceed}
    r = requests.post(requestURL)
    return r

def send_email(userId, collectionId):
    clowder_url = 'http://' + zip_config.clowderhost + ':' + zip_config.clowderport + '/'
    requestURL = clowder_url+'api/admin/mailZip'
    collectionURL = clowder_url+'collection/'+collectionId
    headers = {'Content-type': 'application/json'}
    body = {'collectionId': collectionId, 'userId': userId, 'collectionURL': collectionURL}
    r = requests.post(requestURL,headers=headers,data=json.dumps(body))
    return r

def postCollection(folder, parentId):
    folderName = folder[folder.rfind('/') + 1:]
    print('posting collection with parent',parentId)
    clowderURL = "http://"+zip_config.clowderhost+':'+zip_config.clowderport+"/"
    collectionId = get_collection(clowderURL,zip_config.playserverKey,
                                               folderName,'',parentId)
    return collectionId

def postDataset(folder,parentId):
    folderName = folder[folder.rfind('/')+1:]
    clowderURL = "http://"+zip_config.clowderhost + ':' + zip_config.clowderport+"/"
    datasetId = create_dataset(clowderURL,
                                           folderName,'',None,None,zip_config.playserverKey,parentId )
    print('posting dataset with parent', parentId)
    return datasetId

def postFileToDataset(file,datasetId):
    print('posting file',file,'to dataset',datasetId)
    clowderURL = "http://" + zip_config.clowderhost + ':' + zip_config.clowderport + "/"
    response = upload_files_to_dataset(clowderURL,datasetId,[file],metadata = {},secret_key=zip_config.playserverKey)
    return response

def processFolder(folder, parentId,parameters):
    #time.sleep(0.001)
    contents = os.listdir(folder)
    folderName = folder[folder.rfind('/')+1:]
    files_inside = []
    folders_inside = []
    currentRoot = ""
    for item in contents:
        try:
            if (item !=("__MACOSX" or '.DS_Store')):
                subcontents = os.listdir(folder+'/'+item)
                folders_inside.append(folder+'/'+item)
        except Exception as e:
            print str(e)
            if (item != '.DS_Store'):
                files_inside.append(item)

    #collection case
    if ((len(files_inside) == 0) and (len(folders_inside) > 0)):
        #time.sleep(0.05)
        collectionId = postCollection(folder,parentId)["id"]
        currentRoot = collectionId
        #time.sleep(0.001)
        change_collection_author(parameters,currentRoot)
        for each in folders_inside:
            processFolder(each,collectionId,parameters)
    #dataset case
    elif ((len(files_inside) > 0) and (len(folders_inside) == 0)):
        #time.sleep(0.05)
        datasetId = postDataset(folder,parentId)
        #time.sleep(0.05)
        currentRoot = datasetId
        #time.sleep(0.05)
        change_dataset_author(parameters,currentRoot)
        #time.sleep(0.05)
        for each in files_inside:
            file_id = postFileToDataset(folder+'/'+each,datasetId)
            #time.sleep(0.05)
            change_file_author(parameters,file_id)
            #time.sleep(0.05)
    elif ((len(files_inside) > 0) and (len(folders_inside) > 0)):
        #time.sleep(0.05)
        collectionId = postCollection(folder, parentId)["id"]
        #time.sleep(0.05)
        change_collection_author(parameters,collectionId)
        #time.sleep(0.05)
        currentRoot = collectionId
        datasetId = postDataset(folder, parentId)
        #time.sleep(0.05)
        change_dataset_author(parameters, datasetId)
        #time.sleep(0.05)
        for each in files_inside:
            #time.sleep(0.05)
            file_id = postFileToDataset(folder + "/" + each, datasetId)
            #time.sleep(0.05)
            change_file_author(parameters, file_id)
            #time.sleep(0.05)
        for each in folders_inside:
            processFolder(each,collectionId,parameters)

        #time.sleep(0.05)

    return currentRoot

# ----------------------------------------------------------------------
# find/create the collection in Clowder
# ----------------------------------------------------------------------
def get_collection(clowder_url, secret_key, collection, description='',parentId = ''):
  """Finds or creats a collection in Clowder.

  If the collectin with the given name does not exist yet, it will
  be created. In both cases the collection is returned.

  Args:
      clowder_url: url of Clowder, should end with a /.
      secret_key: key to access Clowder.
      collection: name of collection to find/create.
      description: used when creating the collection.

  Returns:
      The collection found/created or None if it could not be created.

  Raises:
      HttpException: An error occured accessing Clowder.
  """
  log = logging.getLogger(__name__)

  # check to see if collection exists
  # TODO FUNCTION SHOULD NOT USE LIST
  """""
  r = requests.get('%sapi/collections/list?key=%s' %
                   (clowder_url, secret_key))
  if (r.status_code != 200):
    log.error('Could not get list of collections : [%d] - %s)' %
              (r.status_code, r.text))
    return None
  result = r.json()

  if isinstance(result, list):
    for c in result:
      if c['collectionname'] == collection:
        return c

  # create the collection
  """
  collection_id = ""
  if (parentId == ""):
    log.debug('Creating collection "%s".' % collection)
    headers = {'Content-type': 'application/json'}
    body={'name': collection, 'description': description}
    r = requests.post('%sapi/collections?key=%s' %
                    (clowder_url, secret_key),
                    data=json.dumps(body), headers=headers)
    if (r.status_code != 200):
        log.error('Problem creating collection  : [%d] - %s)' %
            (r.status_code, r.text))
        return None
    collection_id = r.json()['id']
  else:
      log.debug('Creating collection "%s".' % collection)
      headers = {'Content-type': 'application/json'}
      body = {'name': collection, 'description': description,'parentId' : parentId}
      r = requests.post('%sapi/collections/newCollectionWithParent?key=%s' %
                        (clowder_url, secret_key),
                        data=json.dumps(body), headers=headers)
      if (r.status_code != 200):
          log.error('Problem creating collection  : [%d] - %s)' %
                    (r.status_code, r.text))
          return None
      collection_id = r.json()['id']


  # add for the next call
  result = r.json()
  return { "id": collection_id, "name": collection, "description": description}

# ----------------------------------------------------------------------
# upload files with optional metadata to existing dataset in clowder
# ----------------------------------------------------------------------
def upload_files_to_dataset(clowder_url, dataset_id, filename_list, metadata={}, username=None, password=None, secret_key=None):
    """
    Upload one or more files to existing dataset, attaching metadata if available.

    Example of uploading 2 files, one with metadata:
        upload_files_to_dataset("http://localhost:9000/", "56747735ea64bf1720266346", ["C:/file1.txt", "C:/file2.txt"],
                                {"file.txt": {"metadata1": 500, "metadata two":"value"}}, "username", "pass")

    :param clowder_url: host URL of target Clowder instance
    :param dataset_id: ID of dataset to attach files to
    :param filename_list: list of filenames to upload e.g. ["MyFile.txt", "MyFile2.txt"]
    :param metadata: JSON object containing metadata; key is filename and value is a JSON metadata object
    :param username: Clowder username
    :param password: Clowder password
    :param secret_key: API access key can be provided instead of username & password
    :return: list of IDs of uploaded files
    """

    log = logging.getLogger(__name__)

    # Prepare the files to be sent
    files_to_send = {}
    for filename in filename_list:
        if not os.path.isfile(filename):
            continue
        f = open(filename, 'rb')
        # for key in body.files, use filename with path omitted
        files_to_send[os.path.basename(filename)] = f

    # Make sure metadata is encoded in proper format for each file
    for md in metadata:
        md_obj = metadata[md]
        # Json metadata for each file's key should be wrapped in a list
        if type(md_obj) is not list:
            if type(md_obj) is not str:
                metadata[md] = [json.dumps(md_obj)]
            else:
                metadata[md] = [md_obj]
        else:
            if type(md_obj) is not str:
                metadata[md] = json.dumps(md_obj)

    # Prepare URL based on auth method
    sess = requests.Session()
    if (secret_key) and not (username and password):
        url_path = '%sapi/uploadToDataset/%s?key=%s' % (clowder_url, dataset_id, secret_key)

    elif (username and password):
        sess.auth = (username, password)
        url_path = '%sapi/uploadToDataset/%s' % (clowder_url, dataset_id)
    else:
        print("Please provide either user credentials or an API access key.")
        return

    r = sess.post(url_path, files=files_to_send, data=metadata)

    if (r.status_code != 200):
        log.error('Problem attaching files  : [%d] - %s)' % (r.status_code, r.text))
        return None

    # Return single ID if single file, or all IDs + filenames if multiple files
    resp_json = r.json()
    if 'id' in resp_json:
        return resp_json['id']
    else:
        return resp_json['ids']


# ----------------------------------------------------------------------
# create a new dataset in clowder and return ID
# ----------------------------------------------------------------------
def create_dataset(clowder_url, name, description, username=None, password=None, secret_key=None,collection = ""):
    """
    Create an empty dataset with given name and description, and return ID.

    :param clowder_url: host URL of target Clowder instance
    :param name: name of Dataset to create
    :param description: description of Dataset to create
    :param username: Clowder username
    :param password: Clowder password
    :param secret_key: API access key can be provided instead of username & password
    :return: ID of dataset that was created
    """

    log = logging.getLogger(__name__)

    # Prepare URL based on auth method
    sess = requests.Session()
    if (secret_key) and not (username and password):
        url_path = '%sapi/datasets/createempty?key=%s' % (clowder_url, secret_key)
    elif (username and password):
        sess.auth = (username, password)
        url_path = '%sapi/datasets/createempty' % (clowder_url)
    else:
        print("Please provide either user credentials or an API access key.")
        return

    if (collection is not ""):
        collectionList = [collection]
        dataDict = dict()
        dataDict["name"] = name
        dataDict["description"] = description
        dataDict["collection"] = [collection]
        dataJson = json.dumps(dataDict)
        r = sess.post(url_path, headers={"Content-Type": "application/json"},
                      data=dataJson)
    else:
        r = sess.post(url_path, headers={"Content-Type":"application/json"},
                  data='{"name":"%s", "description":"%s"}' % (name, description))

    if (r.status_code != 200):
        log.error('Problem creating dataset  : [%d] - %s)' % (r.status_code, r.text))
        return None
    ds_id = r.json()['id']

    return ds_id

def add_collection_to_space(collectionId, spaceId,parameters):
    clowder_url = "http://" + zip_config.clowderhost + ':' + zip_config.clowderport + "/"
    secret_key = zip_config.playserverKey
    r = requests.post('%sapi/spaces/%s/addCollectionToSpace/%s?key=%s' % (clowder_url, spaceId, collectionId, secret_key))
    if (r.status_code != 200):
        # error
        return None
    else:
        return True

import matplotlib
matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt

def getMetadata(input_file):
    metadata = dict()
    metadata['first key'] = 'first value'
    metadata['second key'] = 'second value'
    return metadata

def makePreview(target):
    x = np.linspace(-1, 2, 100)
    y = np.exp(x)
    plt.plot(x, y, color='blue', linestyle='solid')
    plt.savefig(target)
    plt.clf()
